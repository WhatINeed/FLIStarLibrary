// 本地校验
pod lib lint --sources='https://github.com/CocoaPods/Specs.git' --allow-warnings --verbose

//远程校验
pod spec lint --sources='https://github.com/CocoaPods/Specs.git' --allow-warnings --verbose
