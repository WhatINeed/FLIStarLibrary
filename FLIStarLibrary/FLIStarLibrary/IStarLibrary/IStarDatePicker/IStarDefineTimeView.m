//
//  IStarDefineTimeView.m
//  YQMS
//
//  Created by flannery on 2019/7/26.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarDefineTimeView.h"
//#import "FullTimeView.h"
#import "IStarFullTimeView.h"
#import "IStarConstants.h"

@interface IStarDefineTimeView ()<IStarFinishPickView>
@property (nonatomic, strong) UIView * blackMaskView;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) IStarFullTimeView * beginTimeView;
@property (nonatomic, strong) IStarFullTimeView * endTimeView;

@property (nonatomic, strong) NSString * beginTime;
@property (nonatomic, strong) NSString * endTime;

@end

@implementation IStarDefineTimeView

- (instancetype)initWithBegin:(NSString*)beginTime endTime:(NSString*)endTime
{
    self = [super init];
    if (self) {
        CGFloat height = 1225 * WIDTHSCALEISTAR + iPhoneX_SafeArea_Bottom;
        
        if(iOS9){
            height = 1225 * WIDTHSCALEISTAR + iPhoneX_SafeArea_Bottom;
        }else{ // 9以下可以忽略不计
            height = 372 * WIDTHSCALEISTAR + iPhoneX_SafeArea_Bottom + 324;
        }
        
        self.beginTime = beginTime;
        self.endTime = endTime;
        
        [self createMaskView];
        self.frame = CGRectMake(0, SCREENHEIGHTISTAR-height, SCREENWIDTHISTAR, height);
        [self creatCanel];
        [self creatTime];
        
        //设置圆角
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                       byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                                             cornerRadii:CGSizeMake(PT_WIDHTSCALE(12), PT_WIDHTSCALE(12))];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
    }
    return self;
}


//- (instancetype)initWithFrame:(CGRect)frame andBeginTime:(NSString * )beginTime andEndTime:(NSString *)endTime{
//    if (self = [super initWithFrame:frame]) {
//
//
//        self.beginTime = beginTime;
//        self.endTime = endTime;
//
//        [self createMaskView];
//        [[UIApplication sharedApplication].keyWindow addSubview:_maskView];
//        self.frame = frame;
//        [[UIApplication sharedApplication].keyWindow addSubview:self];
//        [self creatCanel];
//        [self creatTime];
//    }
//    return self;
//}
-(void)didFinishPickView:(NSString *)date andType:(NSString *)type
{
    if ([type isEqualToString:@"end"]) {
        self.endTime = date;
    }else{
        self.beginTime = date;
    }
}

- (void)creatTime{
    
    CGFloat yHeight = 132*WIDTHSCALEISTAR; //从线开始
    
    /*
     114
     396 -->510
     36
     114
     396 -->546
     */
    CGFloat beginViewContainerHeight = 510*WIDTHSCALEISTAR;
    CGFloat beginTitleHeight = 114 * WIDTHSCALEISTAR;
    CGFloat beginViewHeight = 396 * WIDTHSCALEISTAR;
    
    CGFloat endViewContainerHeight = 510*WIDTHSCALEISTAR;
    CGFloat endTitleHeight = 114 * WIDTHSCALEISTAR;
    CGFloat endViewHeight = 396 * WIDTHSCALEISTAR;
    
    CGFloat timeMarginLeft = 120*WIDTHSCALEISTAR;
    CGFloat timeMarginRight = 120*WIDTHSCALEISTAR;
    
        // 开始时间
    UIView * beginBgView = [[UIView alloc]initWithFrame:CGRectMake(0, yHeight, SCREENWIDTHISTAR, beginViewContainerHeight)];
    beginBgView.backgroundColor =UIColor.whiteColor;
    [self.bgView addSubview:beginBgView];
    
    UIView *beginLabelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTHISTAR, beginTitleHeight)];
    beginLabelView.backgroundColor = IStarRGBAllColor(0xfafafa);
    [beginBgView addSubview:beginLabelView];
    
    UILabel * beginLabel = [[UILabel alloc]initWithFrame:CGRectMake(36 * WIDTHSCALEISTAR, 0, 218 * WIDTHSCALEISTAR, beginTitleHeight)];
    beginLabel.text = @"开始时间";
    beginLabel.font = [UIFont systemFontOfSize:42 * WIDTHSCALEISTAR];
    beginLabel.textAlignment = NSTextAlignmentLeft | NSTextAlignmentCenter;
    beginLabel.textColor = IStarRGBAllColor(0x333333);
    //beginLabel.backgroundColor = IStarRGBAllColor(0xfafafa);
    [beginBgView addSubview:beginLabel];
    
    self.beginTimeView =[[IStarFullTimeView alloc]initWithFrame:CGRectMake(timeMarginLeft,beginTitleHeight,SCREENWIDTHISTAR - timeMarginLeft - timeMarginRight, beginViewHeight) andTime:self.beginTime];
    self.beginTimeView.curDate=[NSDate date];
    self.beginTimeView.type = @"begin";
    self.beginTimeView.delegate=self;
    [beginBgView addSubview:self.beginTimeView];
    
    // 结束时间
    
    UIView * endBgView = [[UIView alloc]initWithFrame:CGRectMake(0, yHeight+beginViewContainerHeight+36*WIDTHSCALEISTAR, SCREENWIDTHISTAR, endViewContainerHeight)];
    endBgView.backgroundColor = UIColor.whiteColor;
    [self.bgView addSubview:endBgView];
    
    UIView *endLabelView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, SCREENWIDTHISTAR,endTitleHeight)];
    endLabelView.backgroundColor = IStarRGBAllColor(0xfafafa);
    [endBgView addSubview:endLabelView];
    
    UILabel * endLabel = [[UILabel alloc]initWithFrame:CGRectMake(36 * WIDTHSCALEISTAR, 0, 218 * WIDTHSCALEISTAR,endTitleHeight)];
    endLabel.text = @"结束时间";
    endLabel.font = [UIFont systemFontOfSize:42 * WIDTHSCALEISTAR];
    endLabel.textAlignment = NSTextAlignmentLeft | NSTextAlignmentCenter;
    endLabel.textColor = IStarRGBAllColor(0x333333);
    endLabel.backgroundColor =  UIColor.clearColor;
    [endBgView addSubview:endLabel];
    
    self.endTimeView =[[IStarFullTimeView alloc]initWithFrame:CGRectMake(120 * WIDTHSCALEISTAR,endTitleHeight,SCREENWIDTHISTAR - timeMarginLeft - timeMarginRight, endViewHeight) andTime:self.endTime];
    self.endTimeView.curDate=[NSDate date];
    self.endTimeView.type = @"end";
    self.endTimeView.delegate=self;
    [endBgView addSubview:self.endTimeView];
    
}
- (void)creatCanel{
    
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREENWIDTHISTAR, self.bounds.size.height)];
    self.bgView.backgroundColor = IStarRGBAllColor(0xfafafa);
    [self addSubview:self.bgView];
    
    
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREENWIDTHISTAR, 132 * WIDTHSCALEISTAR)];
    titleLabel.text = @"自定义时间";
    titleLabel.font = [UIFont systemFontOfSize:51 * WIDTHSCALEISTAR];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = IStarRGBAllColor(0x222222);
    titleLabel.backgroundColor = IStarRGBAllColor(0xfafafa);
    [self.bgView addSubview:titleLabel];
    
    //右边完成
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = IStarRGBAllColor(0xfafafa);
    btn.frame = CGRectMake(SCREENWIDTHISTAR-(100+36)*WIDTHSCALEISTAR, 0, 100 * WIDTHSCALEISTAR, 132 * WIDTHSCALEISTAR); // y = W - 102
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:IStarRGBAllColor(0x5090F1) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:48 * WIDTHSCALEISTAR];
    [btn addTarget:self action:@selector(cancle) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    
    // 左边取消
    UIButton * btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = CGRectMake(0, 0, 108*WIDTHSCALEISTAR, 132 * WIDTHSCALEISTAR);
    [btnCancel setImage:[UIImage imageNamed:@"filter_close_downarraow"] forState:UIControlStateNormal];
    [btnCancel setImageEdgeInsets:UIEdgeInsetsMake(0, 36 * WIDTHSCALEISTAR, 0, 0)];
    [btnCancel addTarget:self action:@selector(btncancle) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnCancel];
    
    UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, 132 * WIDTHSCALEISTAR, SCREENWIDTHISTAR,0.5)];
    line.backgroundColor = IStarRGBAllColor(0xe6e6e6);
    [self addSubview:line];
    
    UIView * line1 = [[UIView alloc]initWithFrame:CGRectMake(0, self.bounds.size.height - 132 * WIDTHSCALEISTAR - iPhoneX_SafeArea_Bottom, SCREENWIDTHISTAR,0.5)];
    line1.backgroundColor = IStarRGBAllColor(0xe6e6e6);
    [self addSubview:line1];
    
}

/*此时是点击确定*/
- (void)cancle {
    [self hide];
    if(self.definetime) { //通过block回掉
        self.definetime(self.beginTime, self.endTime);
    }
}

/*此时点击取消按钮*/
- (void)btncancle{
    [self hide];
}


-(void)createMaskView{
    //在keyWindow上添加一个带点透明黑色背景
    UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    maskView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureInvoked:)];
    [maskView addGestureRecognizer:singleTap];
    self.blackMaskView = maskView;
}

-(void)singleTapGestureInvoked:(id)sender{
    [self hide];
}


-(void) show{
    CGRect f = self.frame;
    f.origin.y = [UIApplication sharedApplication].keyWindow.bounds.size.height;//self.window.bounds.size.height;
    f.size.width = SCREENWIDTHISTAR;
    self.frame = f;
    
    [[UIApplication sharedApplication].keyWindow addSubview:self.blackMaskView];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    self.blackMaskView.alpha = 1.0f;
    self.blackMaskView.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:0.25f animations:^{
        CGRect fm = self.frame;
        fm.origin.y = fm.origin.y - fm.size.height;
        self.frame = fm;
    } completion:^(BOOL finished) {
        self.blackMaskView.userInteractionEnabled = YES;
    }];
}

-(void)hide{
    CGRect f = self.frame;
    f.origin.y = self.window.bounds.size.height;
    [UIView animateWithDuration:0.25f animations:^{
        self.frame = f;
        self.blackMaskView.alpha = 0.01;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [self.blackMaskView removeFromSuperview];
    }];
}

@end
