//
//  IStarFullTimeView.h
//  YQMS
//
//  Created by flannery on 2019/7/26.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
 //获取的GMT时间，要想获得某个时区的时间，以下代码可以解决这个问题  详情：http://blog.csdn.net/chan1142131456/article/details/50237343
 
 NSTimeZone *zone = [NSTimeZone systemTimeZone];
 NSInteger interval = [zone secondsFromGMTForDate: date];
 NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
 NSLog(@"%@",date);
 NSLog(@"%@", localeDate);
 
 */


@protocol IStarFinishPickView <NSObject>
-(void)didFinishPickView:(NSString*)date andType:(NSString *)type;
@end

@interface IStarFullTimeView : UIView
@property(nonatomic,strong)NSDate*curDate;
@property(nonatomic,strong)id<IStarFinishPickView>delegate;
@property (nonatomic, strong) NSString * type;
-(id)initWithFrame:(CGRect)frame andTime:(NSString *)time;
@end

NS_ASSUME_NONNULL_END
