//
//  IStarDefineTimeView.h
//  YQMS
//
//  Created by flannery on 2019/7/26.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarDefineTimeView : UIView
@property (nonatomic,copy) void(^definetime)(NSString* beginTime, NSString* endTime); //时间选择用的到
//- (instancetype)initWithFrame:(CGRect)frame andBeginTime:(NSString * )beginTime andEndTime:(NSString *)endTime;
- (instancetype)initWithBegin:(NSString*)beginTime endTime:(NSString*)endTime;

-(void) show;
-(void) hide;
@end

NS_ASSUME_NONNULL_END
