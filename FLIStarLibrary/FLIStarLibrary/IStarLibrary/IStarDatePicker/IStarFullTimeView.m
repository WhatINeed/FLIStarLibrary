//
//  IStarFullTimeView.m
//  YQMS
//
//  Created by flannery on 2019/7/26.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarFullTimeView.h"
#import "IStarConstants.h"
#define screenWith  [UIScreen mainScreen].bounds.size.width
#define screenHeight [UIScreen mainScreen].bounds.size.height
@interface IStarFullTimeView() <UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIPickerView*fullPickView;
    
    NSInteger yearRange;
    NSInteger dayRange;
    NSInteger startYear;
    
    NSInteger selectedRowYear;
    NSInteger selectedRowMonth;
    NSInteger selectedRowDay;
    NSInteger selectedRowHour;
    NSInteger selectedRowMinute;
    
    
    
    
    
    
    
    NSInteger selectedYear;
    NSInteger selectedMonth;
    NSInteger selectedDay;
    NSInteger selectedHour;
    NSInteger selectedMinute;
    //    NSInteger selectedSecond;
    //    NSDateFormatter *dateFormatter;
    NSCalendar *calendar;
    
    
}
@property (nonatomic, strong) NSString * time;
@end


@implementation IStarFullTimeView

-(id)initWithFrame:(CGRect)frame andTime:(NSString *)time
{
    if (self=[super initWithFrame:frame]) {
        self.time = time;
        [self config];
    }
    return self;
}
-(void)config
{
    CGFloat perWidth=self.frame.size.width - 36 * WIDTHSCALEISTAR;
    CGFloat height=self.frame.size.height;
    //0
    fullPickView=[[UIPickerView alloc]initWithFrame:CGRectMake(36 * WIDTHSCALEISTAR, 0, perWidth, height)];
    fullPickView.dataSource=self;
    fullPickView.delegate=self;
    [self addSubview:fullPickView];
    
    NSCalendar *calendar0 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    NSDate *cDate = [NSDate date];
    if (self.time) {
        cDate = [formatter dateFromString:self.time];
    }
    comps = [calendar0 components:unitFlags fromDate:cDate];
    NSInteger year=[comps year];
    
    startYear= year-30;
    yearRange = 50;
    selectedYear=2000;
    selectedMonth=1;
    selectedDay=1;
    selectedHour=0;
    selectedMinute=0;
    //    selectedSecond=0;
    dayRange=[self isAllDay:startYear andMonth:1];
    
}
//默认时间的处理
-(void)setCurDate:(NSDate *)curDate
{
    //获取当前时间
    NSCalendar *calendar0 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    
    NSDate *cDate = [NSDate date];
    if (self.time) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd HH:mm"];
        cDate = [formatter dateFromString:self.time];
    }
    if (cDate == nil) {
        cDate = [NSDate date];
    }
    comps = [calendar0 components:unitFlags fromDate:cDate];
    NSInteger year=[comps year];
    NSInteger month=[comps month];
    NSInteger day=[comps day];
    NSInteger hour=[comps hour];
    NSInteger minute=[comps minute];
    //    NSInteger second=[comps second];
    
    selectedYear=year;
    selectedMonth=month;
    selectedDay=day;
    selectedHour=hour;
    selectedMinute=minute;
    //    selectedSecond=second;
    
    dayRange=[self isAllDay:year andMonth:month];
    selectedRowYear = year-startYear;
    selectedRowMonth = month-1;
    selectedRowDay = day-1;
    selectedRowHour = hour;
    selectedRowMinute = minute;
    
    
    [fullPickView selectRow:year-startYear inComponent:0 animated:true];
    [fullPickView selectRow:month-1 inComponent:1 animated:true];
    [fullPickView selectRow:day-1 inComponent:2 animated:true];
    [fullPickView selectRow:hour inComponent:3 animated:true];
    [fullPickView selectRow:minute inComponent:4 animated:true];
    //    [fullPickView selectRow:second inComponent:5 animated:true];
    
    [fullPickView reloadAllComponents];
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
        {
            return yearRange;
        }
            break;
        case 1:
        {
            return 12;
        }
            break;
        case 2:
        {
            return dayRange;
        }
            break;
        case 3:
        {
            return 24;
        }
            break;
        case 4:
        {
            return 60;
        }
            break;
        case 5:
        {
            return 60;
        }
            break;
            
        default:
            break;
    }
    return 0;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 5;
}
-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel*label=[[UILabel alloc]initWithFrame:CGRectMake(158 * WIDTHSCALEISTAR*component, 0,158 * WIDTHSCALEISTAR, 132 * WIDTHSCALEISTAR)];
    label.font=[UIFont systemFontOfSize:30 * WIDTHSCALEISTAR];
    label.tag=component*100+row;
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 8;
    label.textAlignment=NSTextAlignmentCenter;
    switch (component) {
        case 0:
        {
            
            NSString * text = [NSString stringWithFormat:@"%ld年",(long)(startYear + row)];
            
            if (selectedRowYear == row) {
                label.textColor = IStarRGBAllColor(0x222222);
                label.font = [UIFont systemFontOfSize:48 * WIDTHSCALEISTAR];
                
                NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:text];
                
                [AttributedStr addAttribute:NSFontAttributeName
                 
                                      value:[UIFont systemFontOfSize:30 * WIDTHSCALEISTAR]
                                      range:NSMakeRange(text.length-1, 1)];
                
                label.attributedText = AttributedStr;
                
            }else{
                label.text = text;
                //label.textColor = IStarRGBAllColor(0x999999);
                label.textColor = ISTAR_COLOR_999999;
            }
            
            
        }
            break;
        case 1:
        {
            NSString * text = [NSString stringWithFormat:@"%ld月",(long)row+1];
            
            if (selectedRowMonth == row) {
                label.textColor = IStarRGBAllColor(0x222222);
                label.font = [UIFont systemFontOfSize:48 * WIDTHSCALEISTAR];
                
                NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:text];
                
                [AttributedStr addAttribute:NSFontAttributeName
                 
                                      value:[UIFont systemFontOfSize:30 * WIDTHSCALEISTAR]
                                      range:NSMakeRange(text.length-1, 1)];
                
                label.attributedText = AttributedStr;
                
            }else{
                label.text = text;
                label.textColor = ISTAR_COLOR_999999;
                
            }
            //            label.frame=CGRectMake(screenWith/4.0, 0, screenWith/8.0, 30);
        }
            break;
        case 2:
        {
            NSString * text = [NSString stringWithFormat:@"%ld日",(long)row+1];
            if (selectedRowDay == row) {
                label.textColor = IStarRGBAllColor(0x222222);//RGBAllColor(0x222222);
                label.font = [UIFont systemFontOfSize:48 * WIDTHSCALEISTAR];
                
                NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:text];
                
                [AttributedStr addAttribute:NSFontAttributeName
                 
                                      value:[UIFont systemFontOfSize:30 * WIDTHSCALEISTAR]
                                      range:NSMakeRange(text.length-1, 1)];
                
                label.attributedText = AttributedStr;
                
            }else{
                label.text = text;
                label.textColor = IStarRGBAllColor(0x999999);
                
            }
            //            label.frame=CGRectMake(screenWith*3/8, 0, screenWith/8.0, 30);
        }
            break;
        case 3:
            
        {
            NSString * text = [NSString stringWithFormat:@"%ld时",(long)row];
            if (selectedRowHour == row) {
                label.textColor = IStarRGBAllColor(0x222222);
                label.font = [UIFont systemFontOfSize:48 * WIDTHSCALEISTAR];
                
                NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:text];
                
                [AttributedStr addAttribute:NSFontAttributeName
                 
                                      value:[UIFont systemFontOfSize:30 * WIDTHSCALEISTAR]
                                      range:NSMakeRange(text.length-1, 1)];
                
                label.attributedText = AttributedStr;
                
            }else{
                label.text = text;
                label.textColor = IStarRGBAllColor(0x999999);
                
            }
        }
            break;
        case 4:
        {
            NSString * text = [NSString stringWithFormat:@"%ld分",(long)row];
            if (selectedRowMinute == row) {
                label.textColor = IStarRGBAllColor(0x222222);
                label.font = [UIFont systemFontOfSize:48 * WIDTHSCALEISTAR];
                
                NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:text];
                
                [AttributedStr addAttribute:NSFontAttributeName
                 
                                      value:[UIFont systemFontOfSize:30 * WIDTHSCALEISTAR]
                                      range:NSMakeRange(text.length-1, 1)];
                
                label.attributedText = AttributedStr;
                
            }else{
                label.text = text;
                label.textColor = IStarRGBAllColor(0x999999);
                
            }
        }
            break;
            //        case 5:
            //        {
            //            label.textAlignment=NSTextAlignmentRight;
            //            label.frame=CGRectMake(screenWith*component/6.0, 0, screenWith/6.0-5, 30);
            //            label.text=[NSString stringWithFormat:@"%ld秒",(long)row];
            //        }
            //            break;
            //
        default:
            break;
    }
    return label;
}
// 监听picker的滑动
- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    
    switch (component) {
        case 0:
        {
            selectedRowYear = row;
            selectedYear=startYear + row;
            dayRange=[self isAllDay:selectedYear andMonth:selectedMonth];
            [fullPickView reloadComponent:2];
        }
            break;
        case 1:
        {
            selectedRowMonth = row;
            
            selectedMonth=row+1;
            dayRange=[self isAllDay:selectedYear andMonth:selectedMonth];
            if (dayRange < selectedDay) {
                [fullPickView selectRow:dayRange - 1 inComponent:2 animated:NO];
                selectedDay = dayRange;
                selectedRowDay = dayRange - 1;
            }
            [fullPickView reloadComponent:2];
        }
            break;
        case 2:
        {
            selectedRowDay = row;
            
            selectedDay=row+1;
        }
            break;
        case 3:
        {
            selectedRowHour = row;
            
            selectedHour=row;
        }
            break;
        case 4:
        {
            selectedRowMinute = row;
            
            selectedMinute=row;
        }
            break;
            //        case 5:
            //        {
            //            selectedSecond=row;
            //        }
            //            break;
            
        default:
            break;
    }
    [fullPickView reloadAllComponents];   //一定要写这句
    
    NSString*string =[NSString stringWithFormat:@"%ld/%.2ld/%.2ld %.2ld:%.2ld",(long)selectedYear,(long)selectedMonth,(long)selectedDay,(long)selectedHour,(long)selectedMinute];
    
    NSDate * date = [NSDate date];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    [dateFormatter setDateFormat:@"YYYY/MM/dd HH:mm"];
    //一周的秒数
    NSTimeInterval time = 24 * 60 * 60 * 30 ;
    //下周就把"-"去掉
    NSDate *lastWeek = [date dateByAddingTimeInterval:time];
    NSString *startDate =  [dateFormatter stringFromDate:lastWeek];
    
//    if ([self compareDate:string withDate:startDate] == -1) {
//        NSLog(@"时间");
//        [self setCurDate:nil];
//        string = self.time;
//    }
    
    
    if ([self.delegate respondsToSelector:@selector(didFinishPickView:andType:)]) {
        [self.delegate didFinishPickView:string andType:self.type];
    }
}
//比较两个日期大小
-(int)compareDate:(NSString*)startDate withDate:(NSString*)endDate{
    
    int comparisonResult;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY/MM/dd HH:mm"];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] init];
    date1 = [formatter dateFromString:startDate];
    date2 = [formatter dateFromString:endDate];
    
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date2];
    NSDate *localeDate = [date2  dateByAddingTimeInterval: interval];
    
    NSComparisonResult result = [date1 compare:localeDate];
    NSLog(@"result==%ld",(long)result);
    switch (result)
    {
            //date02比date01大
        case NSOrderedAscending:
            comparisonResult = 1;
            break;
            //date02比date01小
        case NSOrderedDescending:
            comparisonResult = -1;
            break;
            //date02=date01
        case NSOrderedSame:
            comparisonResult = 0;
            break;
        default:
            NSLog(@"erorr dates %@, %@", date1, date2);
            break;
    }
    return comparisonResult;
}
- (void)returnToDateWithSelected:(NSString * )selectedStr{
    
}
-(NSInteger)isAllDay:(NSInteger)year andMonth:(NSInteger)month
{
    int day=0;
    switch(month)
    {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            day=31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            day=30;
            break;
        case 2:
        {
            if(((year%4==0)&&(year%100!=0))||(year%400==0))
            {
                day=29;
                break;
            }
            else
            {
                day=28;
                break;
            }
        }
        default:
            break;
    }
    return day;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 132*WIDTHSCALEISTAR;
}
@end
