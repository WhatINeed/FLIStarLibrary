//
//  UIViewIStarTouch.m
//  MSIStarLibrary
//
//  Created by flannery on 2019/12/4.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "UIViewIStarTouch.h"

@implementation UIViewIStarTouch

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (UIColor *)touchesBeganColor{
    if(!_touchesBeganColor) {
        return UIColor.whiteColor;
    }
    return _touchesBeganColor;
}

- (UIColor *)touchesEndColor{
    if(!_touchesEndColor){
        return UIColor.whiteColor;
    }
    return _touchesEndColor;
}

- (UIColor *)touchesCancelColor{
    if(!_touchesCancelColor) {
        return self.touchesEndColor;
    }
    return _touchesCancelColor;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self setBackgroundColor:self.touchesBeganColor];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    [self setBackgroundColor:self.touchesEndColor];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesCancelled:touches  withEvent:event];
    [self setBackgroundColor:self.touchesCancelColor];
}
@end
