//
//  UIViewIStarTouch.h
//  MSIStarLibrary
//
//  Created by flannery on 2019/12/4.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewIStarTouch : UIView

@property (nonatomic, strong) UIColor* touchesBeganColor;
@property (nonatomic, strong) UIColor* touchesEndColor;
@property (nonatomic, strong) UIColor* touchesCancelColor;

@end

NS_ASSUME_NONNULL_END
