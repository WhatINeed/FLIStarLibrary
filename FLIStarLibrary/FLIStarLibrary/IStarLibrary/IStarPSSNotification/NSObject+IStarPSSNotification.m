//
//  NSObject+PSSNotification.m
//  PSSNotification
//
//  Created by 泡泡 on 2018/11/13.
//  Copyright © 2018 泡泡. All rights reserved.
//

#import "NSObject+IStarPSSNotification.h"
#import "IStarPSSEventSet.h"
#import <objc/runtime.h>

static char PSSEventSetKey;

@implementation NSObject (IStarPSSNotification)

- (void)setEventSet:(IStarPSSEventSet *)eventSet {
    objc_setAssociatedObject(self, &PSSEventSetKey, eventSet, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (IStarPSSEventSet *)eventSet {
    IStarPSSEventSet *eventSet = objc_getAssociatedObject(self, &PSSEventSetKey);
    if (!eventSet) {
        eventSet = [[IStarPSSEventSet alloc] init];
        [self setEventSet:eventSet];
    }
    return eventSet;
}

@end














