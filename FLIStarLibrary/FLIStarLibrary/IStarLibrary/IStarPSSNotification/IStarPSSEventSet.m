//
//  PSSEventSet.m
//  PSSNotification
//
//  Created by 泡泡 on 2018/11/13.
//  Copyright © 2018 泡泡. All rights reserved.
//

#import "IStarPSSEventSet.h"
#import "IStarPSSBlockObject.h"
#import "IStarPSSNotificationCenter.h"

@implementation IStarPSSEventSet

- (NSMutableArray<IStarPSSBlockObject *> *)blockObjectArray {
    if (_blockObjectArray == nil) {
        _blockObjectArray = [[NSMutableArray alloc] init];
    }
    return _blockObjectArray;
}

- (void)dealloc {
    //NSLog(@"EventSet 被销毁了");
    [[IStarPSSNotificationCenter defaultCenter] removeObserverByEventSet:self];
}

@end
