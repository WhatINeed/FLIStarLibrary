//
//  PSSEventSet.h
//  PSSNotification
//
//  Created by 泡泡 on 2018/11/13.
//  Copyright © 2018 泡泡. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IStarPSSBlockObject;
@interface IStarPSSEventSet : NSObject

@property (nonatomic, strong) NSMutableArray<IStarPSSBlockObject *> *blockObjectArray;

@end
