//
//  IStarProgressHudManager.h
//  MSIStarLibrary
//
//  Created by iucrzy on 2019/12/17.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarProgressHudManager : NSObject


+ (void)showTextToast:(NSString *)text view:(UIView *)view duration: (NSTimeInterval)duration animated: (BOOL)animated;

+ (void)hideIndicatorInView: (UIView *)view animated: (BOOL)animated;


+ (void)showIndeterminate: (UIView *)view animated: (BOOL)animated;

@end

NS_ASSUME_NONNULL_END
