//
//  IStarProgressHudManager.m
//  MSIStarLibrary
//
//  Created by iucrzy on 2019/12/17.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "IStarProgressHudManager.h"
#import "IStarTools.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation IStarProgressHudManager

+ (void)showTextToast:(NSString *)text view:(UIView *)view duration:(NSTimeInterval)duration animated:(BOOL)animated {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:animated];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.label.numberOfLines = 0;
    hud.removeFromSuperViewOnHide = true;
    [hud hideAnimated:true afterDelay:duration];
}

+ (void)showIndeterminate:(UIView *)view animated:(BOOL)animated {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:animated];
    hud.mode = MBProgressHUDModeCustomView;
    UIView *loadingView = [[UIView alloc] init];
    UIImageView *loadImg = [[UIImageView alloc] init];
    [loadingView addSubview:loadImg];
    loadImg.image = [UIImage imageNamed:@"progress_loading"];
    CABasicAnimation *rotationAnimation;

    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];

    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0];

    rotationAnimation.duration = 0.75;

    rotationAnimation.cumulative = YES;

    rotationAnimation.repeatCount = MAXFLOAT;

    [loadImg.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [loadImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(44);
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(loadingView);
        make.left.mas_equalTo(5);
        make.right.mas_equalTo(-5);
    }];
    UILabel *loadingText = [[UILabel alloc] init];
    [loadingView addSubview:loadingText];
    loadingText.text = @"加载…";
    loadingText.font = [UIFont systemFontOfSize:13];
    loadingText.textColor = IStarRGBAllColor(0x999999);
    [loadingText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(loadImg.mas_bottom).offset(3);
        make.centerX.mas_equalTo(loadingView);
        make.bottom.mas_equalTo(-3);
    }];
    hud.customView = loadingView;
//    [hud.customView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.height.mas_equalTo(100);
//    }];
    hud.backgroundColor = UIColor.whiteColor;
    hud.bezelView.backgroundColor = UIColor.whiteColor;
    hud.removeFromSuperViewOnHide = true;
}


+ (void)hideIndicatorInView:(UIView *)view animated:(BOOL)animated {
    BOOL isSuccess = [MBProgressHUD hideHUDForView:view animated:animated];
}



@end
