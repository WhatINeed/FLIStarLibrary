//
//  IStarPlistTool.h
//  zhwp
//
//  Created by flannery on 2019/2/19.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarPlistTool : NSObject
+(NSMutableDictionary *)getPlistDictionaryNoSuffix:(NSString *)plistName;
+ (NSNumber*)getNumberFromPlist:(NSString*)plistName keyName:(NSString*)keyName;
+ (NSNumber*)getNumberFromPlistDictionary:(NSMutableDictionary*)plistDictionary keyName:(NSString*)keyName;
+(NSDictionary*)getDictionaryFromPlist:(NSString*)plistName keyName:(NSString*)keyName;
@end

NS_ASSUME_NONNULL_END
