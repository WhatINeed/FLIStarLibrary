//
//  IStarTimeUtils.m
//  zhwp
//
//  Created by flannery on 2019/4/8.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import "IStarTimeUtils.h"
#import "IStarTextTools.h"
static NSDateFormatter *cachecdDateFormatter = nil;
@implementation IStarTimeUtils


+(NSDateFormatter*)cachedDateFormatter{
    // If the date formatters aren't already set up, create them and cache them for reuse.
    [IStarTimeUtils cachedDateFormatterWithFormatter:@"yyyy-MM-dd HH:mm:ss"];
    return cachecdDateFormatter;
}

+(NSDateFormatter*)cachedDateFormatterWithFormatter:(NSString*)formatter {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cachecdDateFormatter = [[NSDateFormatter alloc] init];
        [cachecdDateFormatter setLocale:[NSLocale currentLocale]];
    });
    [cachecdDateFormatter setDateFormat:formatter];
    return cachecdDateFormatter;
}

// 先粘过来在说
- (NSDate *) easyDateFormatter{
    time_t t;
    struct tm tm;
    char *iso8601 = "2016-09-18";
    strptime(iso8601, "%Y-%m-%d", &tm);
    tm.tm_isdst = -1;
    tm.tm_hour = 0;//当tm结构体中的tm.tm_hour为负数，会导致mktime(&tm)计算错误
    /**
     //NSString *iso8601String = @"2016-09-18T17:30:08+08:00";
     //%Y-%m-%d [iso8601String cStringUsingEncoding:NSUTF8StringEncoding]
     {
     tm_sec = 0
     tm_min = 0
     tm_hour = 0
     tm_mday = 18
     tm_mon = 9
     tm_year = 116
     tm_wday = 2
     tm_yday = 291
     tm_isdst = 0
     tm_gmtoff = 28800
     tm_zone = 0x00007fd9b600c31c "CST"
     }
     ISO8601时间格式：2004-05-03T17:30:08+08:00 参考Wikipedia
     */
    t = mktime(&tm);
    //http://pubs.opengroup.org/onlinepubs/9699919799/functions/mktime.html
    //secondsFromGMT: The current difference in seconds between the receiver and Greenwich Mean Time.
    return [NSDate dateWithTimeIntervalSince1970:t + [[NSTimeZone localTimeZone] secondsFromGMT]];
}

-(void)convert{
    //YYYY-MM-dd HH:mm:ss
    //2019-06-04 15:15
    NSString *timeStr = [IStarTimeUtils converseTime:@"20190604154239" fromFormatter:@"YYYYMMddHHmmss" toFormatter:@"YYYY-MM-dd HH:mm"];
}

/**
 转换时间

 @param time 时间
 @param from 开始的格式
 @param to 要转换的格式
 */
+(NSString*)converseTime:(NSString*)time fromFormatter:(NSString*)from toFormatter:(NSString*)to{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:from];
    NSDate *date = [dateFormat dateFromString:time];
    
    [dateFormat setDateFormat:to];
    NSString *currentDateStr = [dateFormat stringFromDate:date];
    return currentDateStr;
}


/**
   获取当前的时间
 */
+(NSString*)currentTimeStr:(NSString*)format{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:format]; //@"yyyy/MM/dd HH:mm"
    NSDate *date = [NSDate date];
    NSString * time = [formatter stringFromDate:date];
    return time;
}

/*
 获取LongLongValue的时间
 */
+(NSString*)formatLongLongValue:(NSTimeInterval)time format:(NSString*)format{
    //NSDate *now = [NSDate date];
    //NSDate *now = [[NSDate alloc] initWithTimeIntervalSince1970:time];
    NSDate *now = [NSDate dateWithTimeIntervalSince1970:time/1000];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
//    fmt.dateStyle = kCFDateFormatterShortStyle;
//    fmt.timeStyle = kCFDateFormatterShortStyle;
    //fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    fmt.dateFormat = format;
    NSString* dateString = [fmt stringFromDate:now];
    return dateString;
}

+(NSString*)formatFromDate:(NSDate*)date after:(NSInteger)afterDay format:(NSString*)format{
    NSDate *afterDate;
    if(afterDay != 0) {
        afterDate = [date initWithTimeIntervalSinceReferenceDate:([date timeIntervalSinceReferenceDate]+afterDay*24*3600)];
    } else {
        afterDate = date;
    }
        
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:format];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:afterDate];

    return currentDateStr;
}


+(long)currentTimeInterval{
    NSDate *datenow = [NSDate date];
    return (long)([datenow timeIntervalSince1970] *1000);
}

/*判断两个时间的大小*/
+(BOOL)judgeBigBegin:(NSString*)beginTime endTime:(NSString*)endTime format:(NSString*)format{
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:format];
    NSDate *startD = [date dateFromString:beginTime];
    NSDate *endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = end - start;
    if(value < 0) {
        return  YES; // 开始时间大于结束时间
    } else {
        return  NO;  // 开始时间不大于结束时间
    }
    return NO;
}

+(NSDate *)getDateFromStr:(NSString *)timeStr format:(nonnull NSString *)format{
    if([IStarTextTools isTextEmpty:timeStr]){
        return nil;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter dateFromString:timeStr];
}


// 几天之前 ， 这个时间点到另一个时间点
+(NSString*)beforeDay:(NSInteger)day now:(NSString*)nowTime format:(NSString*)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    // 现在的时间
    NSDate * nowTimeNSDate = [IStarTimeUtils getDateFromStr:nowTime format:format];
    // 几天之前的时间
    NSString* dayString = [dateFormatter stringFromDate: [nowTimeNSDate dateByAddingTimeInterval:day*(-86400)]];
    
    return  dayString;
}


// 几天之前 ， 筛选的前几天
+(NSString*)beforeDayRound:(NSInteger)day now:(NSString*)nowTime{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:format];
//    // 现在的时间
//    NSDate * nowTimeNSDate = [IStarTimeUtils getDateFromStr:nowTime format:format];
    // 几天之前的时间
//    NSString* dayString = [dateFormatter stringFromDate: [nowTimeNSDate dateByAddingTimeInterval:day*(-86400)]];
    //20190803160600 -> 20190803000000
    NSString* returnStr = [NSString stringWithFormat:@"%@ 00:00", [[IStarTimeUtils beforeDay:day now:nowTime format:@"yyyy/MM/dd HH:mm"] substringToIndex:10]];
    return returnStr;
//    return  dayString;
}

//取到今天的末尾
+(NSString*)currentTimeStrCeil{
    NSString* returnStr = [NSString stringWithFormat:@"%@ 23:59", [[IStarTimeUtils currentTimeStr:@"yyyy/MM/dd HH:mm"] substringToIndex:10]];
    return returnStr;
}

+ (NSString *)judegeIsToday:(NSUInteger)time {
    NSString *today = [self currentTimeStr:YYYYMMdd];
    NSString *day = [self formatLongLongValue:time format:YYYYMMdd];
    if ([today isEqualToString:day]) {
        return [self formatLongLongValue:time format:@"HH:mm"];
    }
    return [self formatLongLongValue:time format:@"MM-dd HH:mm"];
}

@end
