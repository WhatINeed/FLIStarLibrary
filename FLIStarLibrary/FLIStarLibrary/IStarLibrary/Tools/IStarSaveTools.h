//
//  IStarSaveTools.h
//  YQMS
//
//  Created by flannery on 2019/7/22.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarSaveTools : NSObject

+(void)saveModelToLocal:(id<NSCoding>)model path:(NSString*)path;
+(id<NSCoding>)getModelFromLocalPath:(NSString*)path;

- (NSString *)getDocumentsPath;

@end

NS_ASSUME_NONNULL_END
