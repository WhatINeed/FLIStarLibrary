//
//  IStarParamsTools.h
//  zhwp
//
//  Created by flannery on 2019/5/7.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarParamsTools : NSObject
+(void)printParamsWithNSDictionary:(NSDictionary*)params;
@end

NS_ASSUME_NONNULL_END
