//
//  IStarTextTools.h
//  zhwp
//
//  Created by flannery on 2019/4/3.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarTextTools : NSObject
+(BOOL)isTextEmpty:(NSString*)text;

+(NSInteger)textLength:(NSString*)text;

+(BOOL)isUrl:(NSString*)url;

+(NSString*)avoidNilString:(NSString*)text;
+(NSString *)avoidNilString:(NSString *)text defaultText:(NSString*)defaultText;
+(NSString*)avoidNilStringWithId:(id)text;

+(NSString*)subStringIfGTLen:(NSString*)text len:(NSInteger)len;

+(NSArray*)participle:(NSString*)str;//分词功能
+(NSInteger) numbersContainOfString:(NSString*)text containsText:(NSString*)containsText;//判断个数

+ (NSString *)descriptionWithLocaleDictionary:(NSDictionary*)dictionary;
+ (NSString *)descriptionWithLocaleArray:(NSArray*)array;

+(NSMutableAttributedString*)attributeText:(NSString*)text textColor:(UIColor*)textColor font:(UIFont*)font;//得到显示颜色的

+ (NSArray<NSString*> *) spliteStr:(NSString*)str split:(NSString*)split;//切分字符串
+ (NSString*)combinArray:(NSArray*)arrayStr split:(NSString*)split;//合并字符串

+ (CGFloat)getWidthWithText:(NSString*)text height:(CGFloat)height font:(CGFloat)font;
+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height andFont:(UIFont*)font;
+ (CGRect) getRectWithText:(NSString*)text height:(CGFloat)height andFont:(UIFont*)font;
@end

NS_ASSUME_NONNULL_END
