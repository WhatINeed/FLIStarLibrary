//
//  IStarBtnTools.h
//  YQMS
//
//  Created by flannery on 2019/8/5.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarBtnTools : NSObject

// 设置attribute文字
+(void)setButton:(UIButton*)btn title:(NSString*)title color:(UIColor*)color font:(UIFont*)font state:(UIControlState)state;
+(void)setNormalButton:(UIButton*)btn title:(NSString*)title color:(UIColor*)color font:(UIFont*)font; //设置正常状态的attribute
+(void)setSelectedButton:(UIButton*)btn title:(NSString*)title color:(UIColor*)color font:(UIFont*)font; //设置选中状态的attribute
@end

NS_ASSUME_NONNULL_END
