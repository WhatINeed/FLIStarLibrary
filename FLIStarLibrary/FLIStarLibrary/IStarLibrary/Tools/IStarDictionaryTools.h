//
//  IStarDictionaryTools.h
//  YQMS
//
//  Created by flannery on 2019/7/29.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface IStarDictionaryTools : NSObject
-(NSDictionary *) properties:(Class)cls; // 获取NSObject的所有属性以及h值
@end

NS_ASSUME_NONNULL_END
