//
//  IStarBtnTools.m
//  YQMS
//
//  Created by flannery on 2019/8/5.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarBtnTools.h"

@implementation IStarBtnTools

// 设置attribute文字
+(void)setButton:(UIButton*)btn title:(NSString*)title color:(UIColor*)color font:(UIFont*)font state:(UIControlState)state{
    if(btn && title) {
        NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:title];
        if(color) {
            [str1 addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, title.length)];
        }
        if(font) {
            [str1 addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, title.length)];
        }
        [btn setAttributedTitle:str1 forState:state];
    }
}


+(void)setNormalButton:(UIButton*)btn title:(NSString*)title color:(UIColor*)color font:(UIFont*)font
{
    [IStarBtnTools setButton:btn title:title color:color font:font state:UIControlStateNormal];
}

+(void)setSelectedButton:(UIButton*)btn title:(NSString*)title color:(UIColor*)color font:(UIFont*)font
{
    [IStarBtnTools setButton:btn title:title color:color font:font state:UIControlStateSelected];
}

@end
