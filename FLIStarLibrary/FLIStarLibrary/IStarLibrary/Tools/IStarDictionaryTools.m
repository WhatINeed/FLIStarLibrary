//
//  IStarDictionaryTools.m
//  YQMS
//
//  Created by flannery on 2019/7/29.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarDictionaryTools.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@implementation IStarDictionaryTools

// 获取这个类的所有属性&值
-(NSDictionary *) properties:(Class)cls{
    unsigned int outCount, index;
    objc_property_t * properties_t = class_copyPropertyList(cls, &outCount);
    NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] init];
    for(index = 0; index < outCount; index++){
        objc_property_t t_property = properties_t[index];
        NSString* attributeName = [NSString stringWithUTF8String:property_getName(t_property)];
        [resultDictionary setObject:[cls valueForKey:attributeName] forKey:attributeName];
    }
//    NSDictionary *res = [NSDictionary dictionaryWithDictionary:resultDictionary];
    return resultDictionary;
}

@end
