//
//  IStarEnvirTools.m
//  YQMS
//
//  Created by flannery on 2019/7/23.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarEnvirTools.h"
#import "IStarMXActionSheet.h"
#import "IStarPlistTool.h"
#import "MBProgressHUD.h"
static NSInteger count;
static BOOL afterLongPress;
static long lastIntervalTime;

@interface IStarEnvirTools () <UIGestureRecognizerDelegate>

@end
@implementation IStarEnvirTools

/**
 添加更改环境的手势
 一次长按，3次短按
 */
-(void)addEvirmentGusture:(UIView*)uiview{
    if(uiview) {
//        uiview.userInteractionEnabled = YES;
//        // 短按
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
//        [uiview addGestureRecognizer:tap];
//
//        UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGr:)];
//        [uiview addGestureRecognizer:longPress];
//
//        [tap requireGestureRecognizerToFail:longPress];
    }
}

+(void)singleTap{
    NSLog(@"singleTap");
    [IStarEnvirTools checkCanDisplayDialog];
    //单击
    if(afterLongPress) {
        //计数
        count++;
        if(count > 2) {
            afterLongPress = NO; //只响应一次
            count=0;
            //此时出发
            NSDictionary *dic = [IStarPlistTool getDictionaryFromPlist:@"Agents" keyName:@"DomainName"];
            [IStarMXActionSheet showWithTitle:@"切换线上环境" cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:[dic allKeys] selectedBlock:^(NSInteger index) {
                //NSLog(@"------> index: %ld", index);
                if(index > 0) {
                    
                    NSDictionary *tmpDict = [IStarPlistTool getDictionaryFromPlist:@"Agents" keyName:@"DomainName"];
                    NSArray *tmpAllKeys = [tmpDict allKeys];
                    NSString *tmpKey = [tmpAllKeys objectAtIndex:(index-1)];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:tmpKey forKey:@"DomainNameId"];
                    //[MBProgressHUD showSuccess:[NSString stringWithFormat:@"环境已切换为: %@", tmpKey]];
                }
            }];
        }
    } else {//清空
        count = 0;
    }
}


+(void)longPressGr{
    NSLog(@"longPressGr");
    lastIntervalTime = [IStarEnvirTools currentTimeInterval];
    // 长按
    afterLongPress = YES;
}

+(void)checkCanDisplayDialog{
    long dd = [IStarEnvirTools currentTimeInterval] - lastIntervalTime;//获取间隔
    //在间隔外，就不能点击
    if(dd < 5000) {
        NSLog(@"5秒之内");
    } else {
        afterLongPress = NO;
        NSLog(@"5秒之外");
    }
    //是否可以显示
}

+(long)currentTimeInterval{
    NSDate *datenow = [NSDate date];
    return (long)([datenow timeIntervalSince1970] *1000);
}

// 切换环境
//+(void)changeEnvirment{
//    //获取Agents.plist文件的路径
//    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Agents" ofType:@"plist"];
//    NSDictionary *dataDictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
//    // DomainNameId DomainName
//    NSString *domainNameId = [self.usernameText.text componentsSeparatedByString:@"切换环境\\"].lastObject;
//    // 如果已知环境id包含输入的id, 则写入到NSUserDefaults
//    if ([[dataDictionary[@"DomainName"] allKeys] containsObject:domainNameId]) {
//        [[NSUserDefaults standardUserDefaults] setObject:domainNameId forKey:@"DomainNameId"];
//        [MSMBProgressHUD showSuccess:[NSString stringWithFormat:@"环境已切换为: %@", domainNameId] toView:self];
//    } else if (domainNameId.length == 0) {
//        // 恢复默认, 清除NSUserDefaults中的DomainNameId
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DomainNameId"];
//        [MSMBProgressHUD showSuccess:[NSString stringWithFormat:@"环境已恢复为: %@", [MSTools getDomainId]] toView:self];
//    }
//}
@end
