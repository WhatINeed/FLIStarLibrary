//
//  IStarConstants.h
//  YQMS
//
//  Created by flannery on 2019/7/26.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define ISTAR_SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define ISTAR_SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define ISTAR_SYSTEM_FONT_PFSM(fontSize)  [UIFont fontWithName:@"PingFangSC-Medium" size: fontSize]
#define ISTAR_SYSTEM_FONT_PFS(fontSize)  [UIFont fontWithName:@"PingFangSC-Regular" size: fontSize]
#define ISTAR_SYSTEM_FONT_PFSS_SCALE(fontSize)  [UIFont fontWithName:@"PingFangSC-Semibold" size: WIDTHSCALE(fontSize)]
#define ISTAR_SYSTEM_FONT_PFSM_SCALE(fontSize)  [UIFont fontWithName:@"PingFangSC-Medium" size: WIDTHSCALE(fontSize)]
#define ISTAR_SYSTEM_FONT_PFS_SCALE(fontSize)  [UIFont fontWithName:@"PingFangSC-Regular" size: WIDTHSCALE(fontSize)]

// 下面是PT格式的,慢慢的演变
#define PT_SYSTEM_FONT_PFSS_SCALE(fontSize)  [UIFont fontWithName:@"PingFangSC-Semibold" size: PT_WIDHTSCALE(fontSize)]
#define PT_SYSTEM_FONT_PFSM_SCALE(fontSize)  [UIFont fontWithName:@"PingFangSC-Medium" size: PT_WIDHTSCALE(fontSize)]
#define PT_SYSTEM_FONT_PFSR_SCALE(fontSize)  [UIFont fontWithName:@"PingFangSC-Regular" size: PT_WIDHTSCALE(fontSize)]


#define IStarRGBAAllColor(rgb, a) [UIColor colorWithRed:((float)((rgb & 0xFF0000) >> 16))/255.0  \
green:((float)((rgb & 0xFF00) >> 8))/255.0     \
blue:((float)(rgb & 0xFF))/255.0              \
alpha:(a)/1.0]
#define IStarRGBAllColor(rgb) IStarRGBAAllColor(rgb, 1.0)
// 字符串转颜色
#define IStarColorWithRGBString(colorString) [UIColor colorWithRed:strtoul([[colorString substringWithRange:NSMakeRange(0, 2)] UTF8String], 0, 16)/255.0 green:strtoul([[colorString substringWithRange:NSMakeRange(2, 2)] UTF8String], 0, 16)/255.0 blue:strtoul([[colorString substringWithRange:NSMakeRange(4, 2)] UTF8String], 0, 16)/255.0 alpha:1];

#define ISTAR_COLOR_007EFF_1 IStarRGBAllColor(0x007EFF)
#define ISTAR_COLOR_007EFF(alpha) IStarRGBAllColor(0x007EFF, alpha)
#define ISTAR_COLOR_3C64FF(alpha) IStarRGBAllColor(0x007EFF, alpha)
#define ISTAR_COLOR_E6E6E6 IStarRGBAllColor(0xE6E6E6)
#define ISTAR_COLOR_666666 IStarRGBAllColor(0x666666)
#define ISTAR_COLOR_333333 IStarRGBAllColor(0x333333)
#define ISTAR_COLOR_999999 IStarRGBAllColor(0x999999)
#define ISTAR_COLOR_FFFFFF IStarRGBAllColor(0xFFFFFF)
#define ISTAR_COLOR_F5F5F5 IStarRGBAllColor(0xF5F5F5)
#define ISTAR_COLOR_F0F0F0 IStarRGBAllColor(0xF0F0F0)
#define ISTAR_COLOR_EDF3FD IStarRGBAllColor(0xEDF3FD)
#define ISTAR_COLOR_5090F1 IStarRGBAllColor(0x5090F1)
#define ISTAR_COLOR_222222 IStarRGBAllColor(0x222222)
#define ISTAR_COLOR_CCCCCC IStarRGBAllColor(0xCCCCCC)

#define SCREENWIDTH [UIScreen mainScreen].bounds.size.width / 375
#define PT_WIDHTSCALE(SS) SS*SCREENWIDTH
#define PT_HEIGHTSCALE(SS) SS*SCREENWIDTH

#define WIDTHSCALEISTAR [UIScreen mainScreen].bounds.size.width / 1080
#define HEIGHTSCALEISTAR WIDTHSCALEISTAR

#define WIDTHSCALE(SS) SS*WIDTHSCALEISTAR
#define HEIGHTSCALE(SS) SS*WIDTHSCALEISTAR

#define SCREENWIDTHISTAR [[UIScreen mainScreen] bounds].size.width
#define SCREENHEIGHTISTAR [[UIScreen mainScreen] bounds].size.height

#define NaviBarHeight  iPhoneX_SafeArea_Top > 20 ? 88.f : 64.f
#define TABBARHEIGHT   iPhoneX_SafeArea_Bottom > 20 ? 83.f : 49.f

// 1. 判断是否为iOS7
#define iOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)
#define IS_OS_8 ([[UIDevice currentDevice].systemVersion floatValue]>=8.0)
#define iOS9 ([[UIDevice currentDevice].systemVersion floatValue]>=9.0)
#define iOS10 ([[UIDevice currentDevice].systemVersion floatValue]>=10.0)
#define IsIOS7Lower ([[[UIDevice currentDevice] systemVersion] floatValue]<7.0) // iOS7以下

// iPhone X底部适配
#define iPhoneX_SafeArea_Top [IStarConstants getSafeAreaTop]
#define iPhoneX_SafeArea_Bottom [IStarConstants getSafeAreaBottom]
#define iPhoneX_SafeArea_Left [IStarConstants getSafeAreaLeft]
#define iPhoneX_SafeArea_Right [IStarConstants getSafeAreaRight]



//#ifdef DEBUG
//#define NSMLog(fmt, ...) NSLog((@"[文件名:%s]\n" "[函数名:%s]\n" "[行号:%d] \n" fmt), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
//#else
//#define MyLog(...)
//#endif

NS_ASSUME_NONNULL_BEGIN

@interface IStarConstants : NSObject
#pragma mark - sareArea
+ (CGFloat)getSafeAreaTop;
+ (CGFloat)getSafeAreaBottom ;
+ (CGFloat)getSafeAreaLeft ;
+ (CGFloat)getSafeAreaRight ;
@end

NS_ASSUME_NONNULL_END
