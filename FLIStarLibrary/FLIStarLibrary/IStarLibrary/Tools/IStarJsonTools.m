//
//  IStarJsonTools.m
//  zhwp
//
//  Created by flannery on 2019/4/8.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import "IStarJsonTools.h"

@implementation IStarJsonTools
+(NSString *)convertToJsonData:(NSDictionary *)dict

{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    
    NSString *jsonString;
    
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    //NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    NSRange range2 = {0,mutStr.length};
    //去掉字符串中的换行符
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    return mutStr;
    
}


+(NSString *)convertToJsonDataWithTrueFalse:(NSDictionary *)dict

{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    
    NSString *jsonString;
    
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    //NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    NSRange range2 = {0,mutStr.length};
    //去掉字符串中的换行符
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    NSRange range3 = {0,mutStr.length};
    [mutStr replaceOccurrencesOfString:@"\"true\"" withString:@"true" options:NSLiteralSearch range:range3];
    NSRange range4 = {0,mutStr.length};
    [mutStr replaceOccurrencesOfString:@"\"false\"" withString:@"false" options:NSLiteralSearch range:range4];
    return mutStr;
    
}



+ (NSString *)toJSONString:(NSArray*)array {
    NSData *data = [NSJSONSerialization dataWithJSONObject:array
                                                   options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                     error:nil];
    if (data == nil) {
        return nil;
    }
    
    NSString *string = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
    return string;
}

+ (NSString *)toReadableJSONString:(NSArray*)array {
    NSData *data = [NSJSONSerialization dataWithJSONObject:array
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:nil];
    if (data == nil) {
        return nil;
    }
    NSString *string = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
    return string;
}

+ (NSData *)toJSONData:(NSArray*)array {
    NSData *data = [NSJSONSerialization dataWithJSONObject:array
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:nil];
    return data;
}



+ (NSDictionary *)parseObjectNonNull:(NSDictionary *)json key:(nonnull NSString *)key {
    NSDictionary *dict= [NSDictionary dictionary];
    if ([json isKindOfClass:[NSDictionary class]]) {//又了判空的操作
        NSDictionary *keyValue =  json[key];
        if ([keyValue isKindOfClass:[NSDictionary class]]) { // 有判断空的操作
            dict = keyValue;
        }
    }
    return dict;
}

+ (NSArray *)parseArrayNonNull:(NSDictionary *)json key:(nonnull NSString *)key {
    NSArray *array = [NSArray array];
    if ([json isKindOfClass:[NSDictionary class]]) {//又了判空的操作
        NSArray *keyValue =  json[key];
        if ([keyValue isKindOfClass:[NSArray class]]) { // 有判断空的操作
            array = keyValue;
        }
    }
    return array;
}


+ (NSString *)parseStringNonNull:(NSDictionary *)json key:(NSString *)key{
    NSString *str = @"";
    if([json isKindOfClass:[NSDictionary class]]) {
        NSString * keyValue = json[key];
        if([keyValue isKindOfClass:[NSString class]]) {
            str = keyValue;
        } else if([keyValue isKindOfClass: [NSNumber class]]) {
            str = [NSString stringWithFormat:@"%lld", keyValue.longLongValue ];
        }
    }
    return str;
}

// 将json字符串解析成字典
+ (NSDictionary*)parseDictionaryNonNull:(NSString*)jsonString{
    if(jsonString) {
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *err;
        NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
        if(err) {
            return [NSDictionary dictionary];
        } else {
            return dic;
        }
    } else {
        return [NSDictionary dictionary];
    }
}

// 将jsonarray 转成NSArray
+ (NSArray*)parseArrayWithJSONStringNonNull:(NSString*)jsonString{
    if(jsonString) {
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *err;
        NSArray* array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
        if(err) {
            return [NSArray array];
        } else {
            return array;
        }
    } else {
        return [NSArray array];
    }
}

// 字典转换成json字符串
+ (NSString*)dictionaryToJSONNonNull:(id)dic{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    if(parseError) {
        return @"";
    } else {
        return [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}















@end
