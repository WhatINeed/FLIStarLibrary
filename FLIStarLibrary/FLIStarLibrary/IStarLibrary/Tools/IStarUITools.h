//
//  IStarUITools.h
//  zhwp
//
//  Created by flannery on 2019/3/28.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//typedef NS_ENUM(NSInteger,UITableViewCellStyle) {UITableViewCellStyleDefault,UITableViewCellStyleValue1,UITableViewCellStyleValue2,UITableViewCellStyleSubtitle};
typedef NS_ENUM(NSInteger, GravityType){
    GRAVITY_LEFT,
    GRAVITY_RIGHT,
    GRAVITY_TOP,
    GRAVITY_BOTTOM
};

@interface IStarUITools : NSObject
//+ (UIView *)addLineToView:(UIView *)view;
+ (void)alpha:(CGFloat)alpha viewContainer:(UIView *)viewContainer;
+ (void)userInteractionEnabled:(BOOL)enabled viewContainer:(UIView *)view;
+ (UIColor *)colorWithHexColorString:(NSString *)hexColorString andAlpha:(CGFloat)alpha;
+ (void) addGestureRecognizer:(UIView*)view target:(id)target selector:(SEL)selector;//添加单击事件
+ (UIView*)addToContainer:(UIImageView*)imageView width:(CGFloat)width height:(CGFloat)height gravityType:(GravityType)type;
+ (UIImageView*)findImageView:(UIView*)viewContainer;

+ (UIView *)addLine; //一条线
+ (UIView *)addLine:(UIColor*)color; //
@end

NS_ASSUME_NONNULL_END
