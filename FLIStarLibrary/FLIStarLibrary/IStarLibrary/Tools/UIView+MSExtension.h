//
//  UIView+MSExtension.h
//  Module_Monitoring
//
//  Created by iucrzy on 2019/10/17.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (MSExtension)

@property (assign, nonatomic) CGFloat ms_x;
@property (assign, nonatomic) CGFloat ms_y;
@property (assign, nonatomic) CGFloat ms_w;
@property (assign, nonatomic) CGFloat ms_h;
@property (assign, nonatomic) CGSize ms_size;
@property (assign, nonatomic) CGPoint ms_origin;

@end

NS_ASSUME_NONNULL_END
