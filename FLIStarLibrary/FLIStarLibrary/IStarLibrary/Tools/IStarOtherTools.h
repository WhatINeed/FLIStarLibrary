//
//  IStarOtherTools.h
//  zhwp
//
//  Created by flannery on 2019/4/25.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarOtherTools : NSObject
+(BOOL) negateBOOL:(BOOL)value;
@end

NS_ASSUME_NONNULL_END
