//
//  IStarCollectionViewTools.h
//  Module_Monitoring
//
//  Created by flannery on 2019/10/16.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarCollectionViewTools : NSObject
+ (void)reloadDataWithView:(UICollectionView*)collectionView;
+ (void)reloadSection:(NSInteger)section collectionView:(UICollectionView*)collectionView;
@end

NS_ASSUME_NONNULL_END
