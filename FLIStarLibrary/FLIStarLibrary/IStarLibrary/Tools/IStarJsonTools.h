//
//  IStarJsonTools.h
//  zhwp
//
//  Created by flannery on 2019/4/8.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarJsonTools : NSObject
+(NSString *)convertToJsonData:(NSDictionary *)dic;
+(NSString *)convertToJsonDataWithTrueFalse:(NSDictionary *)dict;

+ (NSString *)toJSONString:(NSArray*)array;
+ (NSString *)toReadableJSONString:(NSArray*)array ;
+ (NSData *)toJSONData:(NSArray*)array ;

+ (NSDictionary *)parseObjectNonNull:(NSDictionary *)json key:(nonnull NSString *)key;
+ (NSArray *)parseArrayNonNull:(NSDictionary *)json key:(nonnull NSString *)key;
+ (NSString *)parseStringNonNull:(NSDictionary *)json key:(NSString *)key;

+ (NSDictionary*)parseDictionaryNonNull:(NSString*)jsonString;// 将json字符串解析成字典
+ (NSArray*)parseArrayWithJSONStringNonNull:(NSString*)jsonString; // 将jsonarray 转成NSArray
+ (NSString*)dictionaryToJSONNonNull:(id)dic;// 字典转换成json字符串
@end

NS_ASSUME_NONNULL_END
