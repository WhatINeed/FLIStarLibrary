//
//  IStarGestureTools.h
//  Module_Monitoring
//
//  Created by flannery on 2019/10/23.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarGestureTools : NSObject
+ (void)addGestureRecognizer:(UIView*)view target:(id)target selector:(SEL)selector;
+ (void)removeAllGestureRecognizer:(UIView*)view;
@end

NS_ASSUME_NONNULL_END
