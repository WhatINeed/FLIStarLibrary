//
//  IStarPlistTool.m
//  zhwp
//
//  Created by flannery on 2019/2/19.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import "IStarPlistTool.h"

@implementation IStarPlistTool

+(NSMutableDictionary *)getPlistDictionaryNoSuffix:(NSString *)plistName{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    NSMutableDictionary * plistDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    if(plistDictionary) {
        return plistDictionary;
    } else {
        return NSMutableDictionary.new;
    }
}

+ (NSNumber*)getNumberFromPlist:(NSString*)plistName keyName:(NSString*)keyName{
    NSMutableDictionary * array = [IStarPlistTool getPlistDictionaryNoSuffix:plistName];
    return [IStarPlistTool getNumberFromPlistDictionary:array keyName:keyName];
}

+ (NSNumber*)getNumberFromPlistDictionary:(NSMutableDictionary*)plistDictionary keyName:(NSString*)keyName{
    NSNumber * number = [[NSNumber alloc] initWithInt:-1];
//    @"keyName"
//    NSDictionary *value
    if(plistDictionary) {
        number = [plistDictionary objectForKey:keyName];
    }
    return number;
}

+(NSDictionary*)getDictionaryFromPlist:(NSString*)plistName keyName:(NSString*)keyName{
    NSMutableDictionary * plistDictionary = [IStarPlistTool getPlistDictionaryNoSuffix:plistName];
    
    id keyNameId = [plistDictionary objectForKey:keyName];
    if([keyNameId isKindOfClass:[NSDictionary class]]) {
        return (NSDictionary*)keyNameId;
    } else {
        return [NSDictionary dictionary];
    }
}

@end
