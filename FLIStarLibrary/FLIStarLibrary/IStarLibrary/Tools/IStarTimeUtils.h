//
//  IStarTimeUtils.h
//  zhwp
//
//  Created by flannery on 2019/4/8.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define YYYYMMddSub @"YYYY/MM/dd"
#define YYYYMMdd @"yyyyMMdd"
//#define yyyy_MM_dd__ yyyy/MM/dd HH:mm
#define yyyyMMddHHmmSub @"yyyy/MM/dd HH:mm"
#define yyyyMMddHHmmLine @"yyyy-MM-dd HH:mm"
#define yyyyMMddHHmmss @"yyyy-MM-dd HH:mm:ss"
#define YYYYMMddHHmmss_ONLY  @"yyyyMMddHHmmss"

@interface IStarTimeUtils : NSObject
+(NSString*)converseTime:(NSString*)time fromFormatter:(NSString*)from toFormatter:(NSString*)to; //转化时间字符串
+(NSString*)currentTimeStr:(NSString*)format;//获取当前的时间
+(NSString*)formatLongLongValue:(NSTimeInterval)time format:(NSString*)format;

+(NSString*)formatFromDate:(NSDate*)date after:(NSInteger)afterDay format:(NSString*)format;

+(NSDateFormatter*)cachedDateFormatter; //NSDateFormatter
+(NSDateFormatter*)cachedDateFormatterWithFormatter:(NSString*)formatter; //NSDateFormatter
- (NSDate *) easyDateFormatter;
+(long)currentTimeInterval;

+(BOOL)judgeBigBegin:(NSString*)beginTime endTime:(NSString*)endTime format:(NSString*)format;//判断开始时间是否大于结束时间

+(NSDate *)getDateFromStr:(NSString *)timeStr format:(NSString*)format;//根据字符串获取时间
+(NSString*)beforeDay:(NSInteger)day now:(NSString*)nowTime format:(NSString*)format;//几天前的时间
+(NSString*)beforeDayRound:(NSInteger)day now:(NSString*)nowTime;
+(NSString*)currentTimeStrCeil;
+(NSString *)judegeIsToday: (NSUInteger)time;

@end

NS_ASSUME_NONNULL_END
