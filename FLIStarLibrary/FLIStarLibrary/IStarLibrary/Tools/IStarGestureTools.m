//
//  IStarGestureTools.m
//  Module_Monitoring
//
//  Created by flannery on 2019/10/23.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "IStarGestureTools.h"
#import <UIKit/UIKit.h>

@implementation IStarGestureTools
// 点击事件
+ (void)addGestureRecognizer:(UIView*)view target:(id)target selector:(SEL)selector{
    if(view) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
        [view addGestureRecognizer:tapGesture];
        view.userInteractionEnabled = YES;
    }
}

+ (void)removeAllGestureRecognizer:(UIView*)view{
    //[view removeGestureRecognizer:view.gestureRecognizers];
    //view removeGestureRecognizer:view.gestureRecognizers
    for (UIGestureRecognizer * recognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:recognizer];
    }
}

@end
