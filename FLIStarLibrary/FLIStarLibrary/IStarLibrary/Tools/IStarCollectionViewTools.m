//
//  IStarCollectionViewTools.m
//  Module_Monitoring
//
//  Created by flannery on 2019/10/16.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "IStarCollectionViewTools.h"

@implementation IStarCollectionViewTools


+ (void)reloadDataWithView:(UICollectionView*)collectionView{
    if(collectionView) {
        [UIView performWithoutAnimation:^{
            [collectionView reloadData];
        }];
    }
}

+ (void)reloadSection:(NSInteger)section collectionView:(UICollectionView*)collectionView{
    if(collectionView) {
        [UIView performWithoutAnimation:^{
            [collectionView reloadSections:[NSIndexSet indexSetWithIndex:section]];
        }];
    }
}

@end
