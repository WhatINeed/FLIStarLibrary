//
//  IStarHttpTools.m
//  zhwp
//
//  Created by flannery on 2019/4/17.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import "IStarHttpTools.h"
#import "AFNetworking.h"

@implementation IStarHttpTools

+ (BOOL)isStatus0OrMsgSuccess:(NSDictionary *)json {
    /*
     {
     "status":"0",
     "msg":"success",
     "result":{}
     }
     */
    if (json) {
        NSString *status = json[@"status"];
        NSString *msg = json[@"msg"];
        if ([@"0" isEqualToString:status] || [@"success" isEqualToString:msg]) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL)isJson2000AndSuccess:(NSDictionary *)json {
    /*
     {
     "code":"2000"
     "msg":"success"
     "result":null
     }
     */
    if (json) {
        NSString *code = json[@"code"];
        NSString *msg = json[@"success"];
        if ([@"2000" isEqualToString:code] || [@"success" isEqualToString:msg]) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL)isJson0OrOK:(NSDictionary *)json {
    if (json) {
        NSString *code = json[@"code"];
        NSString *msg = json[@"success"];
        if ([@"0" isEqualToString:code] || [@"OK" isEqualToString:msg]) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL)isCode0OrMsgOK:(NSDictionary *)json {
    if (json) {
        NSString *code = [NSString stringWithFormat:@"%@", json[@"code"]];
        NSString *msg = json[@"msg"];
        if ([@"0" isEqualToString:code] || [@"OK" isEqualToString:msg]) {
            return YES;
        }
    }
    return NO;
}

//    "code": 200,"message": "success"
+ (BOOL)isCode200OrMessageSuccess:(NSDictionary *)json {
    if (json) {
        NSString *code = [NSString stringWithFormat:@"%@", json[@"code"]];
        NSString *msg = json[@"message"];
        if ([@"200" isEqualToString:code] || [@"success" isEqualToString:msg]) {
            return YES;
        }
    }
    return NO;
}

+ (void)putWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    //    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf8" forHTTPHeaderField:@"Content-Type"];
        manager.responseSerializer = [AFJSONResponseSerializer new];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript", @"text/plain", nil];
        //manager.requestSerializer.timeoutInterval = AFNTimeoOut;
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];

        [manager.requestSerializer setValue:@"111" forHTTPHeaderField:@"token"];
    [manager PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark - 网络请求
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError *_Nonnull))failure {

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript", @"text/plain", nil];
    //manager.requestSerializer.timeoutInterval = AFNTimeoOut;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [manager.requestSerializer setValue:@"111" forHTTPHeaderField:@"token"];

    [manager GET:url parameters:params progress:^(NSProgress *_Nonnull downloadProgress) {
        //进度
    } success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        if (success) {
//            NSString *base64Decoded = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//            NSData *jsonData = [base64Decoded dataUsingEncoding:NSUTF8StringEncoding];
//
//            NSDictionary * jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
//            NSDictionary * tokenDict = [[NSDictionary alloc] initWithDictionary:jsonObject];
            //NSString * code = [NSString stringWithFormat:@"%@", tokenDict[@"code"]];
            if (success) {
                success(responseObject);
            }
        }
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

// 专门传输Data
+ (void)postWithURL:(NSString *)url data:(NSString *)data success:(void (^)(id _Nonnull))success failure:(void (^)(NSError *_Nonnull))failure {

//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
//    [request setHTTPMethod:@"POST"];
//    [request setValue:@"application/x-www-form-urlencoded"forHTTPHeaderField:@"Contsetent-Type"];
//    if(data) {
//        [request setHTTPBody:data];
//    }
//    
//    NSMutableDictionary *param = NSMutableDictionary.new;
//
//    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    
//    
//    
//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf8" forHTTPHeaderField:@"Content-Type"];
//    manager.responseSerializer = [AFJSONResponseSerializer new];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript", @"text/plain", nil];
//    //manager.requestSerializer.timeoutInterval = AFNTimeoOut;
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//
//    [manager.requestSerializer setValue:@"111" forHTTPHeaderField:@"token"];
}

+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError *_Nonnull))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript", @"text/plain", nil];
    //manager.requestSerializer.timeoutInterval = AFNTimeoOut;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [manager.requestSerializer setValue:@"111" forHTTPHeaderField:@"token"];
    [manager POST:url parameters:params progress:^(NSProgress *_Nonnull uploadProgress) {
        NSLog(@"进度");
    } success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        if (success) {
            //            NSString *base64Decoded = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            //            NSData *jsonData = [base64Decoded dataUsingEncoding:NSUTF8StringEncoding];
            //
            //            NSDictionary * jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
            //            NSDictionary * tokenDict = [[NSDictionary alloc] initWithDictionary:jsonObject];
            //NSString * code = [NSString stringWithFormat:@"%@", tokenDict[@"code"]];
            if (success) {
                success(responseObject);
            }
        }
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void)deleteWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError *_Nonnull))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript", @"text/plain", nil];
    //manager.requestSerializer.timeoutInterval = AFNTimeoOut;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [manager.requestSerializer setValue:@"111" forHTTPHeaderField:@"token"];
    
    /*
     {
         code = 200;
         data = 1;
         message = success;
     }
     */
    [manager DELETE:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            if (success) {
                success(responseObject);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void)patchWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError *_Nonnull))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/javascript", @"text/plain", nil];
    //manager.requestSerializer.timeoutInterval = AFNTimeoOut;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    [manager.requestSerializer setValue:@"111" forHTTPHeaderField:@"token"];
    
    [manager PATCH:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            if (success) {
                success(responseObject);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}


@end
