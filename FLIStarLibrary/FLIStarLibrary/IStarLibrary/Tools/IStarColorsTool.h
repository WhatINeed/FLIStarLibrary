//
//  IStarColorsTool.h
//  YQMS
//
//  Created by flannery on 2019/7/24.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
#define myColor(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
@interface IStarColorsTool : NSObject
// 完美转化为#ffffff格式，暂未发现问题，推荐使用。
// UIColor转#ffffff格式的字符串
+ (NSString *)hexStringFromColor:(UIColor *)color;

+ (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length;

+ (UIColor *)colorWithHexString:(NSString *)hexString;

+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alphaValue;
+ (UIImage*)createImageWithColor:(UIColor*) color;

@end

NS_ASSUME_NONNULL_END
