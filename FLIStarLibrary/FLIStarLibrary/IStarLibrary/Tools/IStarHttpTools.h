//
//  IStarHttpTools.h
//  zhwp
//
//  Created by flannery on 2019/4/17.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarHttpTools : NSObject
+(BOOL)isJson2000AndSuccess:(NSDictionary*)json;
+(BOOL)isJson0OrOK:(NSDictionary*)json;
+(BOOL)isCode0OrMsgOK:(NSDictionary*)json;
+(BOOL)isStatus0OrMsgSuccess:(NSDictionary*)json;
+(BOOL)isCode200OrMessageSuccess:(NSDictionary*)json;

#pragma mark - 网络请求
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure;
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure;
+ (void)putWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure;

#pragma mark - 以下都是未测试
+ (void)deleteWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError *_Nonnull))failure ;
+ (void)patchWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id _Nonnull))success failure:(void (^)(NSError *_Nonnull))failure;
@end

NS_ASSUME_NONNULL_END
