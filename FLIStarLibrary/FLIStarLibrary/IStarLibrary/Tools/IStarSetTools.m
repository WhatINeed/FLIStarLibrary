//
//  IStarSetTools.m
//  YQMS
//
//  Created by flannery on 2019/7/10.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarSetTools.h"

@implementation IStarSetTools

+(BOOL)isNSArrayNil:(NSArray*)array{
    return array == nil;
}

+(BOOL)isNSArrayEmpty:(NSArray*)array{
    if([IStarSetTools isNSArrayNil:array]) {
        return YES;
    }
    if([array count] < 1) {
        return YES;
    }
    return NO;
}

+(BOOL)isIdEmpty:(id)obj{
    return obj == nil;
}

+(BOOL)isNSMutableArrayEmpty:(NSMutableArray*)array{
    if([IStarSetTools isNSArrayNil:array]) {
        return YES;
    }
    if([array count] < 1) {
        return YES;
    }
    return NO;
}

+(BOOL)isNSDictionaryNil:(NSDictionary*)dict{
    return dict == nil;
}

+(BOOL)isNSDictionaryEmpty:(NSDictionary*)dict{
    if([IStarSetTools isNSDictionaryNil:dict]) {
        return YES;
    }
    if([dict allKeys] < 1) {
        return YES;
    }
    return NO;
}
@end
