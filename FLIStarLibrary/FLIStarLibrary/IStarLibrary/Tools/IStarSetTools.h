//
//  IStarSetTools.h
//  YQMS
//
//  Created by flannery on 2019/7/10.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarSetTools : NSObject
+(BOOL)isIdEmpty:(id)obj;
+(BOOL)isNSArrayNil:(NSArray*)array;
+(BOOL)isNSArrayEmpty:(NSArray*)array;

+(BOOL)isNSDictionaryNil:(NSDictionary*)dict;
+(BOOL)isNSDictionaryEmpty:(NSDictionary*)dict;
@end

NS_ASSUME_NONNULL_END
