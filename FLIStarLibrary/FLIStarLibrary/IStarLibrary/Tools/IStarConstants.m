//
//  IStarConstants.m
//  YQMS
//
//  Created by flannery on 2019/7/26.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarConstants.h"

@implementation IStarConstants

#pragma mark - sareArea
+ (CGFloat)getSafeAreaTop {
    if (@available(iOS 11.0, *)) {
        UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
        return insets.top;
    } else {
        return 0;
    }
}
+ (CGFloat)getSafeAreaBottom {
    if (@available(iOS 11.0, *)) {
        UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
        return insets.bottom;
    } else {
        return 0;
    }
}
+ (CGFloat)getSafeAreaLeft {
    if (@available(iOS 11.0, *)) {
        UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
        return insets.left;
    } else {
        return 0;
    }
}
+ (CGFloat)getSafeAreaRight {
    if (@available(iOS 11.0, *)) {
        UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
        return insets.right;
    } else {
        return 0;
    }
}


@end
