//
//  UIView+MSExtension.m
//  Module_Monitoring
//
//  Created by iucrzy on 2019/10/17.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "UIView+MSExtension.h"



@implementation UIView (MSExtension)
- (CGFloat)ms_x {
    return self.frame.origin.x;
}

- (void)setMs_x:(CGFloat)ms_x {
    CGRect frame = self.frame;
    frame.origin.x = ms_x;
    self.frame = frame;
}

- (void)setMs_y:(CGFloat)ms_y {
    CGRect frame = self.frame;
    frame.origin.y = ms_y;
    self.frame = frame;
}
- (CGFloat)ms_y {
    return self.frame.origin.y;
}

- (void)setMs_w:(CGFloat)ms_w {
    CGRect frame = self.frame;
    frame.size.width = ms_w;
    self.frame = frame;
}

- (CGFloat)ms_w {
    return self.frame.size.width;
}

- (void)setMs_h:(CGFloat)ms_h {
    CGRect frame = self.frame;
    frame.size.height = ms_h;
    self.frame = frame;
}

- (CGFloat)ms_h {
    return self.frame.size.height;
}

- (void)setMs_size:(CGSize)ms_size {
    CGRect frame = self.frame;
    frame.size = ms_size;
    self.frame = frame;
}

- (CGSize)ms_size {
    return self.frame.size;
}

- (void)setMs_origin:(CGPoint)ms_origin {
    CGRect frame = self.frame;
    frame.origin = ms_origin;
    self.frame = frame;
}

- (CGPoint)ms_origin {
    return self.frame.origin;
}
@end
