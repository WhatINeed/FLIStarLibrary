//
//  IStarParamsTools.m
//  zhwp
//
//  Created by flannery on 2019/5/7.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import "IStarParamsTools.h"

@implementation IStarParamsTools
+(void)printParamsWithNSDictionary:(NSDictionary*)params{
    if(params)
        for (NSString * key in params) {
            NSLog(@"IStarParamsTools-->key: %@ value: %@", key, params[key]);
        }
}

@end
