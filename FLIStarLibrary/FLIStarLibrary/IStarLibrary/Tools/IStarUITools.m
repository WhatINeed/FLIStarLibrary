//
//  IStarUITools.m
//  zhwp
//
//  Created by flannery on 2019/3/28.
//  Copyright © 2019年 zhxg. All rights reserved.
//

#import "IStarUITools.h"
#import "IStarConstants.h"
#import "Masonry.h"

@implementation IStarUITools
//+ (UIView *)addLineToView:(UIView *)view {
//    UIView *line = [[UIView alloc] init];
//    line.backgroundColor = GRAY_LINECOLOR;
//    [view addSubview:line];
//    return line;
//}

+ (void)alpha:(CGFloat)alpha viewContainer:(UIView *)viewContainer{
    for(UIView *view in viewContainer.subviews) {
        if(view) {
            view.alpha = alpha;
        }
    }
    viewContainer.alpha = alpha;
}

+ (void)userInteractionEnabled:(BOOL)enabled viewContainer:(UIView *)viewContainer{
    for(UIView *view in viewContainer.subviews) {
        if(view) {
            view.userInteractionEnabled = enabled;
        }
    }
    viewContainer.userInteractionEnabled = enabled;
}

+ (UIColor *)colorWithHexColorString:(NSString *)hexColorString andAlpha:(CGFloat)alpha {
    if ([hexColorString length] <6){//长度不合法
        return [UIColor clearColor];
    }
    NSString *tempString=[hexColorString lowercaseString];
    if ([tempString hasPrefix:@"0X"]){//检查开头是0x
        tempString = [tempString substringFromIndex:2];
    }
    else if ([tempString hasPrefix:@"#"]){//检查开头是#
        tempString = [tempString substringFromIndex:1];
    }
    if ([tempString length] != 6){
        return [UIColor clearColor];
    }
    
    //分解三种颜色的值
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    // r
    NSString *rString = [tempString substringWithRange:range];
    // g
    range.location = 2;
    NSString *gString = [tempString substringWithRange:range];
    // b
    range.location = 4;
    NSString *bString = [tempString substringWithRange:range];
    //取三种颜色值
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString]scanHexInt:&r];
    [[NSScanner scannerWithString:gString]scanHexInt:&g];
    [[NSScanner scannerWithString:bString]scanHexInt:&b];
    return [UIColor colorWithRed:((float) r /255.0f)
                           green:((float) g /255.0f)
                            blue:((float) b /255.0f)
                           alpha:alpha];
    
}


#pragma mark ----imageview------
//-(void)imageViewPadding{
//
//}

// 点击事件
+ (void)addGestureRecognizer:(UIView*)view target:(id)target selector:(SEL)selector{
    if(view) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
        [view addGestureRecognizer:tapGesture];
        view.userInteractionEnabled = YES;
    }
}

#pragma mark 添加容器,以便于扩大点击范围,
+ (UIView*)addToContainer:(UIImageView*)imageView width:(CGFloat)width height:(CGFloat)height gravityType:(GravityType)type{
    UIView *view = UIView.new;
    if(imageView) {
        [view addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            if(type == GRAVITY_RIGHT) {
                make.right.mas_equalTo(view.mas_right);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(height);
                make.centerY.mas_equalTo(view.mas_centerY);
            } else if(type == GRAVITY_LEFT) {
                make.left.mas_equalTo(view.mas_left);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(height);
                make.centerY.mas_equalTo(view.mas_centerY);
            } else if(type == GRAVITY_TOP) {
                make.top.mas_equalTo(view.mas_top);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(height);
                make.centerX.mas_equalTo(view.mas_centerX);
            } else if(type == GRAVITY_BOTTOM) {
                make.bottom.mas_equalTo(view.mas_bottom);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(height);
                make.centerX.mas_equalTo(view.mas_centerX);
            }
        }];
    }
    
    return view;
}

#pragma mark 找到容器内第一个ImageView, 默认就一个View
+ (UIImageView*)findImageView:(UIView*)viewContainer {
    for(UIView *subView in viewContainer.subviews) {
        if([subView isKindOfClass:[UIImageView class]]) {
            return (UIImageView*) subView;
        }
    }
    return UIImageView.new;
}

// 添加一条线
+ (UIView *)addLine{
    return [self addLine:ISTAR_COLOR_CCCCCC];
}

// 添加一条线
+ (UIView *)addLine:(UIColor*)color{
    UIView *view = UIView.new;
    view.backgroundColor = color;
    return view;
}


@end
