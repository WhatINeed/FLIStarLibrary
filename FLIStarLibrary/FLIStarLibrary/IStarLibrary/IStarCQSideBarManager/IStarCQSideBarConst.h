//
//  CQSideBarConst.h
//  CQSideBarManager
//
//  Created by heartjoy on 2017/3/7.
//  Copyright © 2017年 heartjoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CQExtension.h"

#define ISTAR_SIDEBAR_START_POINT CGPointMake(70.f, 0.f)

#define ISTAR_SIDEBAR_ANIMATE_DURATION 0.25f

#define ISTAR_SIDEBAR_COLOR(r,g,b,a) [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:a]
#define ISTAR_SIDEBAR_RANDOM_COLOR  ISTAR_SIDEBAR_COLOR(arc4random_uniform(256),arc4random_uniform(256),arc4random_uniform(256),1)

#define ISTAR_SIDEBAR_SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define ISTAR_SIDEBAR_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define SIDEBAR_KEY_WINDOW [UIApplication sharedApplication].keyWindow

UIKIT_EXTERN NSString *const CQSideBarOpenErrorText;
