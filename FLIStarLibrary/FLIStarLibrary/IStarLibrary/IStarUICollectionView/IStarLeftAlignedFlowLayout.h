//
//  UICollectionViewLeftAlignedLayout.h
//  MSIStarLibrary
//
//  Created by flannery on 2019/11/12.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarLeftAlignedFlowLayout : UICollectionViewFlowLayout

@end

/**
 *  Just a convenience protocol to keep things consistent.
 *  Someone could find it confusing for a delegate object to conform to UICollectionViewDelegateFlowLayout
 *  while using UICollectionViewLeftAlignedLayout.
 */
@protocol IStarDelegateLeftAlignedLayout <UICollectionViewDelegateFlowLayout>

@end

NS_ASSUME_NONNULL_END
