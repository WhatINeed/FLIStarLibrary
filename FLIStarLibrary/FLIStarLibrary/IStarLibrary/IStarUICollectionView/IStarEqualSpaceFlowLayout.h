//
//  IStarEqualSpaceFlowLayout.h
//  YQMS
//
//  Created by flannery on 2019/7/25.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, IStarAlignType){
    IStarAlignWithLeft,
    IStarAlignWithCenter,
    IStarAlignWithRight
};

@interface IStarEqualSpaceFlowLayout : UICollectionViewFlowLayout

// 两个Cell之间的距离
@property (nonatomic, assign) CGFloat betweenOfCell;
//cell对齐方式
@property (nonatomic, assign) IStarAlignType cellType;
//-(instancetype) initWithType:(IStarAlignType)cellType;

@end

NS_ASSUME_NONNULL_END
