//
//  IStarSelectRegionModel.m
//  YQMS
//
//  Created by flannery on 2019/8/14.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import "IStarSelectRegionModel.h"

@implementation IStarSelectRegionModel

/*
 code;
 name;
 cities;
 province;
 city;
 */

- (instancetype)initCode:(NSString*)code name:(NSString*)name cities:(NSArray*)cities province:(BOOL)province city:(BOOL)city
{
    self = [super init];
    if (self) {
        self.code = code;
        self.name = name;
        self.cities = cities;
        self.province = province;
        self.city = city;
    }
    return self;
}



- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.code forKey:@"code"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.cities forKey:@"cities"];
    [aCoder encodeBool:self.province forKey:@"province"];
    [aCoder encodeBool:self.city forKey:@"city"];
}
// 解档
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.code = [aDecoder decodeObjectForKey:@"code"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.cities = [aDecoder decodeObjectForKey:@"cities"];
        self.province = [aDecoder decodeBoolForKey:@"province"];
        self.city = [aDecoder decodeBoolForKey:@"city"];
    }
    return self;
}
@end
