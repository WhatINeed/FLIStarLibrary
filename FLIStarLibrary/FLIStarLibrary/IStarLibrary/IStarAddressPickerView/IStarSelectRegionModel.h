//
//  IStarSelectRegionModel.h
//  YQMS
//
//  Created by flannery on 2019/8/14.
//  Copyright © 2019年 yqzbw. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarSelectRegionModel : NSObject
@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSArray *  cities;
@property (nonatomic, assign) BOOL       province;
@property (nonatomic, assign) BOOL       city;

- (instancetype)initCode:(NSString*)code name:(NSString*)name cities:(NSArray*)cities province:(BOOL)province city:(BOOL)city;
@end

NS_ASSUME_NONNULL_END
