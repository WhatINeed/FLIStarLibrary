//
//  JJOptionView.h
//  DropdownListDemo
//
//  Created by 俊杰  廖 on 2018/9/20.
//  Copyright © 2018年 HoYo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JJOptionViewElement;

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE

@class IStarJJOptionView;

@protocol JJOptionViewDelegate <NSObject>

@optional

- (void)optionView:(IStarJJOptionView *)optionView selectedIndex:(NSInteger)selectedIndex;

@end

@interface IStarJJOptionView : UIView
/**
 标题名
 */
@property (nonatomic, strong) IBInspectable NSString *title;

/**
 标题颜色
 */
@property (nonatomic, strong) IBInspectable UIColor *titleColor;

/**
 标题字体大小
 */
@property (nonatomic, assign) IBInspectable CGFloat titleFontSize;

/**
 视图圆角
 */
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

/**
 视图边框颜色
 */
@property (nonatomic, strong) IBInspectable UIColor *borderColor;

/**
 边框宽度
 */
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;

/**
 cell高度
 */
@property (nonatomic, assign) CGFloat rowHeigt;
/**
 
 */
@property (nonatomic, assign) CGFloat tableViewOffset;

/**
 数据源
 */
@property (nonatomic, strong) NSArray<JJOptionViewElement*> *dataSource;

/**
 * 设置默认的index
 */
@property (nonatomic, strong) NSIndexPath *currentIndexPath;

@property (nonatomic, weak) id<JJOptionViewDelegate> delegate;

@property (nonatomic,copy) void(^selectedBlock)(IStarJJOptionView *optionView,NSInteger selectedIndex);
@property (nonatomic,copy) void(^hasChangedBlock)(IStarJJOptionView *optionView,NSInteger selectedIndex);
 //偷懒了，不建议这样使用

//- (void)setDefaultSelected:(NSIndexPath *)indexPath;//默认位置
//- (void)setDefaultSelectedText:(NSString*)text;//默认字段

- (instancetype)initWithFrame:(CGRect)frame dataSource:(NSArray<JJOptionViewElement*> *)dataSource;

-(void)setCurrentView:(IStarJJOptionView*)view;
-(void)setCurrentElement:(JJOptionViewElement*)element;
-(JJOptionViewElement*)getCurrentIndexValue;
- (JJOptionViewElement*)getCurrentElement:(NSInteger)index;
-(id)getCurrentIndexPresent;

@end

@interface JJOptionViewElement : NSObject <NSCoding>
@property (nonatomic,strong) NSString *displayTitle;//显示的内容
@property (nonatomic,strong) id present;//代表的数据

- (instancetype)initWithDisplayTitle:(NSString*)displayTitle present:(id)present;
@end

NS_ASSUME_NONNULL_END
