# 介绍
下拉框
# 使用

```
IStarJJOptionView *view = IStarJJOptionView.new;
view.dataSource = @[@"全文",@"标题",@"作者",@"来源",@"精准", @"模糊"];
view.selectedBlock = ^(IStarJJOptionView * _Nonnull optionView, NSInteger selectedIndex) {
NSLog(@"%@",optionView);
NSLog(@"%ld",selectedIndex);
};
view.currentIndexPath = [NSIndexPath indexPathForRow:3 inSection:0];
[self.view addSubview:view];
[view mas_makeConstraints:^(MASConstraintMaker *make) {
//make.center.mas_equalTo(self.view.center);
make.centerY.mas_equalTo(self.view.mas_centerY);
make.left.mas_equalTo(self.view).offset(100);
make.width.mas_equalTo(100);
make.height.mas_equalTo(40);
}];
```
