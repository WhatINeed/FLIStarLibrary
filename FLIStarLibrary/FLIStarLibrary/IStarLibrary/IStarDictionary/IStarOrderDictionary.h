//
//  IStarOrderDictionary.h
//  MSIStarLibrary
//
//  Created by flannery on 2019/11/12.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarOrderDictionary : NSMutableDictionary
{
    NSMutableDictionary *dictionary;
    NSMutableArray *array;
}

- (NSArray *)keyArray;
- (NSArray *)valueArray;
- (IStarOrderDictionary*)addKey:(id)aKey value:(id)value;
- (IStarOrderDictionary*)addObject:(id)anObject forKey:(id)aKey;
// 按顺序插入
//- (NSInteger)insertObject:(id)anObject forKey:(id)aKey;
//插入一个位置
- (void)insertObject:(id)anObject forKey:(id)aKey atIndex:(NSInteger)anIndex;
- (void)replaceObject:(id)anObject forKey:(id)aKey; //替换某个key值
//取得某个位置的obj
- (id)keyAtIndex:(NSUInteger)anIndex;
- (id)valueAtIndex:(NSUInteger)anIndex;
- (NSInteger)indexForKey:(NSString *)key;
//逆序
- (NSEnumerator *)reverseKeyEnumerator;
//顺序
- (NSEnumerator *)keyEnumerator;
//过滤中文等字符
- (void)filtrateChinese;
@end

NS_ASSUME_NONNULL_END
