//
//  IStarOrderDictionary.m
//  MSIStarLibrary
//
//  Created by flannery on 2019/11/12.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "IStarOrderDictionary.h"

NSString * DescriptionForObject(NSObject *object, id locale, NSUInteger indent)
{
    NSString *objectString;
    if ([object isKindOfClass:[NSString class]]) {
        objectString = [[NSString alloc]init];
    } else if ([object respondsToSelector:@selector(descriptionWithLocale:indent:)]) {
        objectString = [(NSDictionary *)object descriptionWithLocale:locale indent:indent];
    } else if ([object respondsToSelector:@selector(descriptionWithLocale:)]) {
        objectString = [(NSSet *)object descriptionWithLocale:locale];
    } else {
        objectString = [object description];
    }
    return objectString;
}

@implementation IStarOrderDictionary

//初始化方法
- (id)init
{
    self = [super init];
    if (self != nil) {
        dictionary = [NSMutableDictionary dictionary];
        array = [NSMutableArray array];
    }
    return self;
}

//初始化方法
//- (id)initWithCapacity:(NSUInteger)capacity
//{
//    self = [super init];
//    if (self != nil) {
//        dictionary = [[NSMutableDictionary alloc] initWithCapacity:capacity];
//        array = [[NSMutableArray alloc] initWithCapacity:capacity];
//    }
//    return self;
//}

- (NSArray *)keyArray {
    return array;
}

// 获取所有的标题
- (NSArray *)valueArray{
    NSArray *array = [self keyArray];
    NSMutableArray* valueArray = [NSMutableArray array];
    
    for (id key in array) {
        [valueArray addObject: [self valueForKey:key]];
    }
    return valueArray;
}

//copy方法
- (id)copy
{
    return [self mutableCopy];
}

//复写方法
- (void)setObject:(id)anObject forKey:(id)aKey
{
    if (![dictionary objectForKey:aKey]) {
        [array addObject:aKey];
    }
    [dictionary setObject:anObject forKey:aKey];
}

- (void)removeObjectForKey:(id)aKey
{
    [dictionary removeObjectForKey:aKey];
    [array removeObject:aKey];
}

- (NSUInteger)count
{
    return [dictionary count];
}

- (id)objectForKey:(id)aKey
{
    return [dictionary objectForKey:aKey];
}

- (NSEnumerator *)keyEnumerator
{
    return [array objectEnumerator];
}

- (NSEnumerator *)reverseKeyEnumerator
{
    return [array reverseObjectEnumerator];
}

//纯属为了方便
- (IStarOrderDictionary *)addKey:(id)aKey value:(id)value {
    return [self addObject:value forKey:aKey];
}

// 往后加+1
- (IStarOrderDictionary *)addObject:(id)anObject forKey:(id)aKey {
    //[self insertObject:anObject forKey:aKey];//依次添加数据
    
    // 还得有去重的条件
    if([array containsObject:aKey]){
        // 就不添加了
    } else {
        [array insertObject:aKey atIndex:array.count];
        [dictionary setObject:anObject forKey:aKey];
    }
    return self;
}

//- (NSInteger)insertObject:(id)anObject forKey:(id)aKey {
//    NSInteger index = array.count;
//    [self insertObject:anObject forKey:aKey atIndex:index];//依次添加数据
//    return index;
//}

// 替换
- (void)replaceObject:(id)anObject forKey:(id)aKey{
    if([[dictionary allKeys] containsObject:aKey])
    {
        [dictionary setObject:anObject forKey:aKey];
    } else {
        //添加
        [self addKey:aKey value:anObject];
    }
}

- (void)insertObject:(id)anObject forKey:(id)aKey atIndex:(NSInteger)anIndex
{
    if([array containsObject:aKey]) {
        // 先不改变位置了， 现在没这个需求
    } else if(anIndex > -1 && anIndex < array.count) {
        // 直接加到最前边
        [array insertObject:aKey atIndex:anIndex];
    } else {
        //往后加
        //[array insertObject:aKey atIndex:array.count];
        [array addObject:aKey];
    }
    [dictionary setObject:anObject forKey:aKey];
}

//- (id)keyAtIndex:(NSUInteger)anIndex
//{
//    NSInteger index =    [array objectAtIndex:anIndex];
//    if (index >= 0 && index < array.count) {
//        return -1;
//    } else {
//        return index;
//    }
//    //return [array objectAtIndex:anIndex];
//}

- (NSInteger)indexForKey:(NSString *)key {
    NSInteger pos = -1;
    
    for (NSInteger i=0; i<array.count; i++) {
        id valueKey = [array objectAtIndex:i];
        if ([valueKey isKindOfClass:[NSString class]]
            && [valueKey isEqualToString:key]) {
            return i; //找到key的位置
        }
    }
    return pos;
}

//获取值
- (id)valueAtIndex:(NSUInteger)anIndex {
    id aKey =    [array objectAtIndex:anIndex];
    return [dictionary objectForKey:aKey];
}

- (id)valueForKey:(NSString *)key {
//    [dictionary objectForKey:key];
    return [dictionary valueForKey:key];
}

- (void)removeAllObjects{
    [array removeAllObjects];
    [dictionary removeAllObjects];
}

- (void)dealloc
{
    [array removeAllObjects];
    [dictionary removeAllObjects];
}

//返回一个字符串对象,该对象代表了字典的内容,格式的属性列表。
- (NSString *)descriptionWithLocale:(id)locale indent:(NSUInteger)level
{
    NSMutableString *indentString = [NSMutableString string];
    NSUInteger i, count = level;
    for (i = 0; i < count; i++) {
        [indentString appendFormat:@"    "];
    }

    NSMutableString *description = [NSMutableString string];
    [description appendFormat:@"%@{\n", indentString];
    for (NSObject *key in self) {
        [description appendFormat:@"%@    %@ = %@;\n",
         indentString,
         DescriptionForObject(key, locale, level),
         DescriptionForObject([self objectForKey:key], locale, level)];
    }
    [description appendFormat:@"%@}\n", indentString];
    return description;
}

//过滤中文等字符
- (void)filtrateChinese
{
    for (id key in self.allKeys) {
        id object = [self objectForKey:key];
        if ([object isKindOfClass:[NSString class]]) {
            [self setObject:(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)object, NULL, NULL, kCFStringEncodingUTF8)) forKey:key];
        }
    }
}

@end
