//
//  IStarCacheManager.m
//  MSIStarLibrary
//
//  Created by flannery on 2019/12/4.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "IStarCacheManager.h"
#import "IStarSaveTools.h"

@implementation IStarCacheManager

+ (void)saveDictionary:(NSDictionary *)dictionary key:(NSString *)key {
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        [self saveObj:dictionary key:key];
    } else {
        @throw [NSException exceptionWithName:@"不是NSDictionary" reason:@"不是NSDictionary" userInfo:nil];
    }
}

+ (void)saveArray:(NSArray *)array key:(NSString *)key {
    if ([array isKindOfClass:[NSArray class]]) {
        [self saveObj:array key:key];
    } else {
        @throw [NSException exceptionWithName:@"不是NSArray" reason:@"不是NSArray" userInfo:nil];
    }
}

+ (void)saveObj:(id<NSCoding>)obj key:(NSString *)key {
    [IStarSaveTools saveModelToLocal:obj path:key];
}

+ (NSDictionary *)readDictionaryFromKey:(NSString *)key {
    return [self readObjFromKey:key];
}

+ (NSArray *)readArrayFromKey:(NSString *)key {
    return [self readObjFromKey:key];
}

+ (id)readObjFromKey:(NSString *)key {
    return [IStarSaveTools getModelFromLocalPath:key];
}

/**
 1. 统一的失效
 2. 个别的失效
 */
+ (BOOL)isValidateNetworkRequest {
    return YES;
}

// 判断字典是否有效
+ (BOOL)isDictionaryValidate:(NSDictionary *)dictionary {
    if (dictionary && [dictionary isKindOfClass:[NSDictionary class]]) {
        return YES;
    }
    return NO;
}

// 判断NSArray是否有效
+ (BOOL)isArrayValidate:(NSArray *)array {
    if (array && [array isKindOfClass:[NSArray class]]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isObjValidate:(id)obj {
    if (obj) {
        return YES;
    }
    return NO;
}

@end
