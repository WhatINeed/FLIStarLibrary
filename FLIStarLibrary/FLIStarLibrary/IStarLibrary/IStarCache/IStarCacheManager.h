//
//  IStarCacheManager.h
//  MSIStarLibrary
//
//  Created by flannery on 2019/12/4.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IStarCacheManager : NSObject

+ (void)saveDictionary:(NSDictionary*)dictionary key:(NSString*)key;
+ (void)saveArray:(NSArray*)array key:(NSString*)key;
+ (NSDictionary*)readDictionaryFromKey:(NSString*)key;
+ (NSArray*)readArrayFromKey:(NSString*)key;
+ (BOOL)isValidateNetworkRequest;

+ (BOOL)isDictionaryValidate:(NSDictionary*)dictionary;
+ (BOOL)isArrayValidate:(NSArray*)array;
+ (BOOL)isObjValidate:(id)obj;

@end

NS_ASSUME_NONNULL_END
