//
//  IStarMXActionManager.h
//  Module_Monitoring
//
//  Created by flannery on 2019/10/11.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol IStarMXActionManagerDelegate <NSObject>

@required
// 侧边栏里面具体的View
-(UIView*)viewForActionView;


@end

@interface IStarMXActionManager : NSObject

@property (nonatomic, assign) CGFloat height;//弹出试图的高度

+(instancetype)sharedInstance;

//弹出来
-(void)openActionView:(id<IStarMXActionManagerDelegate>)delegate;
//关闭
-(void)closeActionView;

@end

NS_ASSUME_NONNULL_END
