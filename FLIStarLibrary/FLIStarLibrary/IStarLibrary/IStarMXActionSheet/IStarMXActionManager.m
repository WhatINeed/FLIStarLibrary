//
//  IStarMXActionManager.m
//  Module_Monitoring
//
//  Created by flannery on 2019/10/11.
//  Copyright © 2019 com.isstashine.www. All rights reserved.
//

#import "IStarMXActionManager.h"
#import "IStarTools.h"
#import "IStarConstants.h"

NSInteger const animDuration = 0.55f;

@interface IStarMXActionManager ()
{
    UIView *_contentView;
}

@property (nonatomic, strong) UIWindow *currentWindow;
@property (nonatomic, strong) UIView *shadeView;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@property (nonatomic, weak) id<IStarMXActionManagerDelegate> delegate;
@end
@implementation IStarMXActionManager

+ (instancetype)sharedInstance
{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    self.height = [UIScreen mainScreen].bounds.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height - 44;

    _currentWindow = [UIApplication sharedApplication].keyWindow;
}

- (void)openActionView:(id<IStarMXActionManagerDelegate>)delegate {
    _delegate = delegate;
    if (_contentView) {
        [_contentView removeFromSuperview];
        _contentView = nil;
    }
    if (_shadeView) {
        [_shadeView removeFromSuperview];
        _shadeView = nil;
    }

    if (!_shadeView) {
        CGPoint origin = _currentWindow.frame.origin;
        CGSize size = _currentWindow.frame.size;
        CGFloat satusHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        _shadeView = [[UIView alloc] initWithFrame:CGRectMake(origin.x, origin.y + satusHeight, size.width, size.height)];
        _shadeView.backgroundColor = UIColor.blackColor;

        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureInvoked:)];
        [_shadeView addGestureRecognizer:singleTap];
    }
    [_currentWindow addSubview:_shadeView];

    if (_delegate) {
        if ([_delegate respondsToSelector:@selector(viewForActionView)]) {
            _contentView = [self.delegate viewForActionView];
        }
    }
    [_currentWindow addSubview:_contentView];
    [self openAnimation];
}

- (void)closeActionView {
    if (_contentView) {
        [self closeAnimation];
    }
}

#pragma mark - gesture actions
- (void)singleTapGestureInvoked:(UITapGestureRecognizer *)recognizer {
    [self closeActionView];
}

- (void)openAnimation {
    self->_shadeView.alpha = 0.01f;
    self->_contentView.frame = CGRectMake(0, SCREENHEIGHTISTAR, SCREENWIDTHISTAR, self.height);//初始的位置
    [UIView animateWithDuration:animDuration animations:^{
        //在动画过程中禁止遮罩视图响应用户手势
//        self->_maskView.alpha = 1.0f;
//        self->_maskView.userInteractionEnabled = NO;

//        CGRect frame = self.frame;
//        frame.origin.y = SCREENHEIGHTISTAR - self.frame.size.height;//self.frame.size.height;
//        self.frame = frame;
//        self.contentView.frame = frame;

        if (self.shadeView) {
            self.shadeView.alpha = 0.6f;
            self.shadeView.userInteractionEnabled = NO;
        }
        if (self->_contentView) {
            self->_contentView.frame = CGRectMake(0, SCREENHEIGHTISTAR - self.height, SCREENWIDTHISTAR, self.height);
        }
    } completion:^(BOOL finished) {
        //在动画结束后允许遮罩视图响应用户手势
        self.shadeView.userInteractionEnabled = YES;
    }];
}

- (void)closeAnimation {
    [UIView animateWithDuration:animDuration animations:^{
        if (self->_contentView) {
            self->_contentView.frame = CGRectMake(0, SCREENHEIGHTISTAR, SCREENWIDTHISTAR, self.height);    //初始的位置
        }
        if (self.shadeView) {
            self.shadeView.alpha = 0.01f;
        }
    } completion: ^(BOOL finished) {
        //        if(self.contentView) {
        //            [self.contentView removeFromSuperview];
        //        }
        //        [self removeFromSuperview];
        //        [self->_maskView removeFromSuperview];
        //[self removeFromSuperview];

        if (self->_shadeView) {
            [self->_shadeView removeFromSuperview];
        }
        if (self->_contentView) {
            [self->_contentView removeFromSuperview];
        }
        self->_shadeView = nil;
        self->_contentView = nil;
        self->_delegate = nil;
    }];
}

@end
