
Pod::Spec.new do |spec|
  spec.name         = "FLIStarLibrary"
  spec.version      = "0.0.1"

  spec.summary      = "A short description of FLIStarLibrary."

  spec.description  = <<-DESC
                FLIStarLibrary component
                   DESC

  spec.homepage     = "https://gitee.com/WhatINeed/FLIStarLibrary.git"

  spec.license      = "MIT"

  spec.author       = { "flannery" => "18310579837@163.com" }

  spec.platform     = :ios, "9.0"

  spec.source       = { :git => "https://gitee.com/WhatINeed/FLIStarLibrary.git", :tag => "#{spec.version}" }

  spec.source_files  = "FLIStarLibrary/FLIStarLibrary/**/*.{h,m}"
  spec.resource = 'FLIStarLibrary/FLIStarLibrary/Resource/*.png'

  spec.framework  = "Foundation"
  spec.static_framework  =  true
  spec.dependency 'Masonry', '~> 1.1.0'
  spec.dependency 'MBProgressHUD', '~>1.1.0'
  spec.dependency "AFNetworking", "~>3.2.1"
end
