//
//  AppDelegate.h
//  FLIStarLibrary
//
//  Created by flannery on 2020/2/19.
//  Copyright © 2020 flannery. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

